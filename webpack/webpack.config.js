const path                   = require('path');
const webpack                = require('webpack');
const MiniCssExtractPlugin   = require('mini-css-extract-plugin');
const WebpackAssetsManifest  = require('webpack-assets-manifest');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const BrowserSyncPlugin      = require('browser-sync-webpack-plugin');
const TerserPlugin           = require('terser-webpack-plugin');
const CssMinimizerPlugin     = require('css-minimizer-webpack-plugin');
// const IgnoreAssetsWebpackPlugin = require('ignore-assets-webpack-plugin');
module.exports = ( env ) => {
    let config = {
        // cache: false,
        mode: 'development',
        entry: {
            admcustom: path.resolve(__dirname, 'src/js/admin-custom.js'),
            home: path.resolve(__dirname, 'src/js/home.js'),
            shop: path.resolve(__dirname, 'src/js/shop.js'),
            'single-product': path.resolve(__dirname, 'src/js/single-product.js'),
            cart: path.resolve(__dirname, 'src/js/cart.js'),
            checkout: path.resolve(__dirname, 'src/js/checkout.js'),
            myacc: path.resolve(__dirname, 'src/js/myacc.js'),
            'other-pages': path.resolve(__dirname, 'src/js/other-pages.js'),
            commons: path.resolve(__dirname, 'src/js/commons.js'),
        },
        devtool: 'eval-cheap-source-map',//'eval-cheap-module-source-map',
        output: {
            filename: 'js/[name].[contenthash].bundle.js',
            chunkFilename: 'js/[name].[contenthash].bundle.js',
            path: path.resolve(__dirname, '../assets'),
            publicPath: '../'
        },
        optimization: {
            minimize: true,
            minimizer: [new TerserPlugin(), new CssMinimizerPlugin()],
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        chunks: 'all',
                    },
                },
            }
        },
        module: {
            rules: [
                {
                    test: /\.(jpe?g|png)$/i,
                    use: {
                            loader: 'url-loader',
                            options: {
                                name: 'img/[name].[hash].[ext]',
                                limit: 25000,
                            }
                        }
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use:{
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[hash].[ext]'
                        }
                    }
                },
                {
                    test: /\.(jpe?g|png|gif)$/i,
                    use: {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                enabled: true,
                            },
                            pngquant: {
                                quality: [0.65, 0.90],
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                            webp: {
                                quality: 75
                            }
                        }
                    }
                },
                {
                    test: /\.css$/i,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('autoprefixer')({}),
                                    require('precss'),
                                ]
                            },
                        },
                        'sass-loader'

                    ],
                },
            ],
        },
        plugins: [
            new BrowserSyncPlugin({
                watch: true,
                host: 'localhost',
                port: 8080,
                proxy: "http://letmebuy.ru",
                files: [
                    {
                        match: [
                            "/var/www/letmebuy.ru/wp-content/themes/letmebuy/**/*.php",
                            "/var/www/letmebuy.ru/wp-content/themes/letmebuy/**/*.html",
                            "/var/www/letmebuy.ru/wp-content/themes/letmebuy/**/*.js"
                        ],
                    }
                ]
            }),
            new MiniCssExtractPlugin({
                filename: 'css/[name].[contenthash].css'
            }),
            new WebpackAssetsManifest({
                // Options go here
            }),
            new CleanWebpackPlugin(
                {
                    cleanOnceBeforeBuildPatterns: ['js/*', 'css/*', 'img/*', '!js/libraries'],
                }
            ),
        ]
    };
    let babel = {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
                presets: [
                    [
                        "@babel/preset-env", {
                            modules: false,
                            useBuiltIns: "usage",
                            corejs: 3,
                        }
                    ]
                ],
                plugins: ["@babel/plugin-syntax-dynamic-import"]
            }
        }
    };
    config.module.rules.push( babel );

    return config;
};