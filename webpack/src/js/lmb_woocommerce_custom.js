jQuery(function($) {
    $(document.body).on('wc_cart_button_updated', function( e, $button ) {
        $button.next().addClass('lmb-button lmb-button__add-to-cart lmb-button_regular');
    });
});