import '../css/cart.css';
$( document ).ready( function() {
    $('.lmb-input__plus').click( function() {
        disableUpdateButton();
    } );
    $('.lmb-input__min').click( function() {
        disableUpdateButton();
    } );
} );
function disableUpdateButton() {
    $( '.woocommerce-cart-form :input[name="update_cart"]' ).prop( 'disabled', false ).attr( 'aria-disabled', false );
}