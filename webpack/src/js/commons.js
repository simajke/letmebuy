import Spinner from './../img/spinner1.gif';
// import '../css/commons.css';
import './parts/header-search';
import {getSpinner} from './functions';
$( document ).ready(function() {
    initialzieCommons();
});

function initialzieCommons() {
    initializeButtons();
    initializeEvents();
}
function initializeEvents() {
    $( document.body ).on( 'updated_wc_div', function() {
        initializeButtons();
        $( '.woocommerce-cart-form' ).find( ':input[name="update_cart"]' ).prop( 'disabled', false ).attr( 'aria-disabled', false );
    } );

    $( document.body ).on( 'adding_to_cart', function( e, data ) {
        $(data[0]).parent().find('.lmb-button__add-to-cart').hide();
        $(data[0]).removeClass('d-inline-flex').hide().after( getSpinner( Spinner ) );
    });

    $( document.body ).on( 'added_to_cart', function( e, fragments, cart_hash, button ) {
        let spinner = $(button).parent().find('.spinner-wrapper');
        $(spinner).remove();
        $(button).addClass('d-inline-flex').show();
        $(button).parent().find('.added_to_cart').show();
    });
}
function initializeButtons() {
    $('.lmb-input__plus').click(function(e) {
        e.preventDefault();
        let inputToIncrement = $( this ).closest('.quantity').find('input[type=number]');
        if ( !inputToIncrement.attr('max') || +inputToIncrement.attr('value') < +inputToIncrement.attr('max') ) {
            inputToIncrement.attr('value', +inputToIncrement.attr('value') + 1);
        }
    });

    $('.lmb-input__min').click(function(e) {
        e.preventDefault();
        let inputToIncrement = $( this ).closest('.quantity').find('input[type=number]');
        if ( +inputToIncrement.attr('value') > +inputToIncrement.attr('min') ) {
            inputToIncrement.attr('value', +inputToIncrement.attr('value') - 1);
        }
    });
}