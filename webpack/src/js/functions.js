export function getSpinner( spinnerPath ) {
    let spinnerWrapper = document.createElement("div");
    spinnerWrapper.className = "spinner-wrapper";
    let imgLoader = new Image();
    imgLoader.className = "img-fluid lmb-spinner";
    imgLoader.src = window.lmb_ajax.assets_path + 'img/' + basePath( spinnerPath );

    spinnerWrapper.appendChild(imgLoader);
    return spinnerWrapper;
}
function basePath(path) {
    return path.split( '/' ).pop();
}