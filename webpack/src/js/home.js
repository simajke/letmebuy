import "core-js/modules/es.promise";
import "core-js/modules/es.array.iterator";
import './lmb_woocommerce_custom';
import '../css/home.css';
import '../img/logo.svg';
import '../img/ph-head.svg';
import '../img/search.svg';
import '../img/liked.svg';
import '../img/cart.svg';
import '../img/cart-white.svg';
import '../img/hbanner.svg';
import '../img/delivery.svg';
import '../img/quality.svg';
import '../img/moneyback.svg';
import '../img/vaacum.jpg';
import '../img/starb.svg';
import '../img/center-banner.svg';
import '../img/apple.svg';
import '../img/dji.svg';
import '../img/xiaomi.svg';
import '../img/moza.svg';
import '../img/freevision.svg';
import '../img/zhiyun.svg';
import '../img/moza-banner.svg';
$(function() {
    init();
});

function init() {
    $('.main-slick__choose-brand' ).wrap('<div id="categorizedSliderWrapper"></div>');
    slick_slider_main_banner();
    slick_slider_buyers_choice();
    slick_slider_choose_brand();
    slick_slider_choose_brand_products();
    categorized_slider_ajax();
}
function slick_slider_choose_brand() {
    $('.brands-row__brand-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        // slidesToShow: 6,
        slidesToScroll: 3,
        variableWidth: true,
        arrows: true,
        draggable: false,
        prevArrow: $(".brands-row__arrows .lmb-prev-arrow"),
        nextArrow: $(".brands-row__arrows .lmb-next-arrow"),
    });
}
function slick_slider_main_banner() {
    $('.home__main-banner_context').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // autoplay: true,
        // autoplaySpeed: 4000,
    });
}
function slick_slider_choose_brand_products() {
    $('.main-slick__choose-brand').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 2,
        arrows: false,
        variableWidth: true,
        centerMode: true
    });
}
function slick_slider_buyers_choice() {
    $('.main-slick__buyers-choice').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 2,
        arrows: false,
        variableWidth: true,
        centerMode: true,
    });
}
function categorized_slider_ajax() {
    $('.brands-row__brand-wrapper').on('click', function() {
        let data = {
            action: 'categorized_slider',
            category_slug: $(this).prop('id').split("-")[1]
        }
        $.post(
            lmb_ajax.url,
            data,
            function(response) {
                $('#categorizedSliderWrapper').html( response.slider );
                slick_slider_choose_brand_products();
            }
        );
    });
}