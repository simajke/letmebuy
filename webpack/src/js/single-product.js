import '../img/starb.svg';
import '../img/starg.svg';
import '../css/single-product.css';
$( document ).ready(function() {
    setMainImageUnclickable();
    setSliderMainImage();
    updateCommentRating();
});

function updateCommentRating() {
    $( 'body' ).on('init', function() {
        let starsWrapper = $('#review_form_wrapper').find('.stars span');
        starsWrapper.html( starsWrapper.find('a').toArray().reverse() );
    });
}
function setSliderMainImage() {
    $('.lmb-single-product__thumbnails-wrapper').find('a').on('click', function(e) {
        e.preventDefault();
        let id = $( this ).children().attr('id');
        let data = {
            action: 'lmb_get_attachment_html',
            attachment_id: id.split("-")[1]
        }

        $.post(
            lmb_ajax.url,
            data,
            function( response ) {
                $( '#lmb-single-product__image-wrapper' ).html( response.markup );
                setMainImageUnclickable();
            }
        );
    });
}
function setMainImageUnclickable() {
    $('.lmb-single-product__image-wrapper').find('a').on('click',function(e) {
        e.preventDefault();
    });
}