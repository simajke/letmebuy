$(document).ready(function() {
    let searchResultElement = document.getElementById('lmb-search-response');
    let lmbProductSearch = $( "#lmb-product-search" );
    let searchClear = $('#lmb-product-search__clear');

    //in case user click outside of input field we still have to keep focus on it
    $('.modal-content').on('click', function() {
        lmbProductSearch.focus();
    });

    //when 'clear([X]) button' clicked input cleared
    searchClear.on('click', function() {
        $(this).siblings('#lmb-product-search').val('');
        lmbProductSearch.trigger('input');
    });


    $('.lmb-search-label').on('click', function() {
        $( "#trigger-test" ).trigger( "click" );
        setTimeout( function() {
            lmbProductSearch.focus();
        }, 500);
    });

    $('#staticBackdrop').on('click keydown', function() {
        setTimeout( clearSearchInput.bind( this ), 50, searchResultElement );
    });

    lmbProductSearch.on('input', function(e) {
        if ( this.value.length ) {
            replaceClass( searchClear, 'd-none', 'd-flex' );
        } else {
            replaceClass( searchClear, 'd-flex', 'd-none' );
        }

        if ( this.value.length > 2 ) {
            let data = {
                action: 'lmb_get_search_result',
                search_word: this.value
            }

            $.post(
                lmb_ajax.url,
                data,
                function( response ) {
                    if ( typeof response.markup !== 'undefined' ) {
                        removeElementClassName( searchResultElement, 'd-none' );
                        searchResultElement.innerHTML = response.markup.trim();
                    }
                }
            );
        } else {
            addElementClassName( searchResultElement, 'd-none' );
        }
    });
});

function removeElementClassName( el, className ) {
    if ( el.classList.contains( className ) ) {
        el.classList.remove( className );
    }
}

function addElementClassName( el, className ) {
    if ( ! el.classList.contains( className ) ) {
        el.classList.add( className );
    }
}

function clearSearchInput( searchInput ) {
    if ( ! $( this ).hasClass( "show" ) ) {
            $('#lmb-product-search').val('');
            searchInput.innerHTML = '';
            addElementClassName( searchInput, 'd-none' );
    }
}

function replaceClass( element, classRemove, classAdd ) {
    if ( $( element ).hasClass( classRemove ) ) {
        element.removeClass( classRemove );
        element.addClass( classAdd );
    }
}