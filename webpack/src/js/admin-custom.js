import '../css/admstyle.css';
import '../img/start.svg';
import '../img/stary.svg';
jQuery(function($) {
    //Stars for "Product" posts start
    if ( $( '.comment-reply' ).length ) {
        let rating = `<fieldset>
            <legend>
                <span>Add Rating to new Comment</span>
            </legend>
            <legend id="custom-rating">
                <input id="one" type="radio" name="rating" value="1">
                <label for="one"></label>
                <input id="two" type="radio" name="rating" value="2">
                <label for="two"></label>
                <input id="three" type="radio" name="rating" value="3">
                <label for="three"></label>
                <input id="four" type="radio" name="rating" value="4">
                <label for="four"></label>
                <input id="five" type="radio" name="rating" value="5" checked>
                <label for="five"></label>
            </legend>
        </fieldset>`;

        $( rating ).insertBefore( '.comment-reply' );

        $('#custom-rating').on('click', function( e ) {
            let commentsSection = $('.comment-reply');
            let activeRatingInput = '';
            let appendRatingField = '';

            if ( e.currentTarget == e.target ) {
                activeRatingInput = $( '#' + this.id + ' > input:checked');
            } else {
                activeRatingInput = $( e.target );
            }

            if ( activeRatingInput.length ) {
                appendRatingField = `<input type="hidden" name="rating" id="rating" value="${activeRatingInput.val()}">`;
            }

            if ( appendRatingField ) {
                if ( $('.comment-reply > #rating').length ) {
                    commentsSection.find('#rating').remove();
                }
                commentsSection.append( appendRatingField );
            }
        });

        $( "#custom-rating" ).trigger( "click" );
    }
    //Stars for "Product" posts end

    //"Tag" taxonomy checkboxes start
    let customCheckboxes = $( '.lmb-checkbox-listener' );
    let form = $( '#addtag' );
    if ( ! form.length ) {
        form = $( '#edittag' );
    }
    if ( ! form.length ) {
        return;
    }
    let submit = $( form ).find( 'input[type=submit]' );
    if ( customCheckboxes.length && submit.length ) {
        $( customCheckboxes ).click( function() {
            let hidden = $( this ).closest('.form-field').find('input[type=hidden]');
            setOnclickHiddenInputCheckbox( hidden );
        });

        $( submit ).click( function() {
            if ( $( customCheckboxes ).prop( "checked" ) === true ) {
                $( customCheckboxes ).prop( "checked", false );
            }
        } );
    }
    function setOnclickHiddenInputCheckbox( element ) {
        let name = $( element ).prop('name');
        if( $( element ).prop('value') === '1' ) {
            $( element ).prop('value', '0');
        } else {
            $( element ).prop('value', '1');
        }
    }
    //"Tag" taxonomy checkboxes end
});