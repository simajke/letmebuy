<?php
defined( 'ABSPATH' ) || exit;
global $wp;
$values = array();
if ( isset( $_GET['orderby'] ) ) {
    $values['orderby'] = lmb_sanitize_text_field( wp_unslash( $_GET['orderby'] ) );
}
if ( empty( $value ) ) {
    $values = null;
}
if ( '' === get_option( 'permalink_structure' ) ) {
    $form_action = remove_query_arg( array( 'page', 'paged', 'product-page' ), add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) );
} else {
    $form_action = preg_replace( '%\/page/[0-9]+%', '', home_url( trailingslashit( $wp->request ) ) );
}
?>
<div class="col-3 order-0">
    <div class="lmb-filter lmb-filter__wrapper">
        <form id="lmb-filter-form" action="<?php echo esc_url( $form_action ) ?>" method="get">
            <?php dynamic_sidebar('product-filter'); ?>
            <button class="lmb-button lmb-button_regular">Фильтровать</button>
            <?php wc_query_string_form_fields( $values, array( 'manufact', 'category', 'max_price', 'min_price', 'submit', 'paged', 'products_availability', 'product-page' ) ); ?>
        </form>
    </div>
</div>