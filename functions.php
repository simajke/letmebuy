<?php
class LMB_Theme {
    /**
     * @var LMB_Theme
     */
    private static $lmb;

    /**
     * @var LMB_Query
     */
    public $query;


    /**
     * @var LMB_Cart
     */
    public $cart;

    /**
     * @var LMB_Checkout
     */
    public $checkout;

    /**
     * @var LMB_My_Account
     */
    public $my_account;

    /**
     * @var LMB_Tag_Taxonomy_Customizer
     */
    public $taxonomy_customizer;

    /**
     * @var LMB_Initializer_Factory is related to widgets
     */
    public $initializer_factory;

    private function __construct() {
        $this->init();
    }

    public static function get_theme_object() {
        if ( is_null( self::$lmb ) ) {
            self::$lmb = new LMB_Theme();
        }
        return self::$lmb;
    }

    private function init() {
        $this->set_constants();
        $this->includes();
        $this->set_components();
    }

    private function set_constants() {
        if ( ! defined( 'LMB_PATH' ) ) {
            define( 'LMB_PATH', dirname( __FILE__ ) );
        }

        if ( ! defined( 'LMB_MANIFEST_PATH' ) && file_exists( $manifest = get_stylesheet_directory() . '/assets/manifest.json' ) ) {
            define( 'LMB_MANIFEST_PATH', $manifest );
        }

        if ( ! defined( 'LMB_ASSETS_PATH' ) ) {
            define( 'LMB_ASSETS_PATH', get_stylesheet_directory_uri() . '/assets/' );
        }
    }

    private function includes() {
        //interfaces
        include_once LMB_PATH . '/includes/components/widgets/factory/interface-lmb-initializer-factory.php';
        //abstract
        include_once LMB_PATH . '/includes/components/widgets/initializers/abstract-lmb-initializer.php';
        include_once LMB_PATH . '/includes/components/sliders/abstract-lmb-slider.php';
        //classes
        include_once LMB_PATH . '/includes/components/class-lmb-query.php';
        include_once LMB_PATH . '/includes/components/class-lmb-tag-taxonomy-customizer.php';
        include_once LMB_PATH . '/includes/components/class-lmb-cart.php';
        include_once LMB_PATH . '/includes/components/class-lmb-checkout.php';
        include_once LMB_PATH . '/includes/components/class-lmb-my-account.php';
//        include_once LMB_PATH . '/includes/components/lmb-styles.php';
        include_once LMB_PATH . '/includes/components/widgets/factory/class-lmb-initializer-factory.php';
        include_once LMB_PATH . '/includes/components/widgets/initializers/class-lmb-product-in-stock-checker-initializer.php';
        include_once LMB_PATH . '/includes/components/widgets/initializers/class-lmb-filter-categories-initializer.php';
        include_once LMB_PATH . '/includes/components/widgets/initializers/class-lmb-filter-tag-initializer.php';
        include_once LMB_PATH . '/includes/components/widgets/customizers/class-lmb-filter-tag-customizer.php';
        include_once LMB_PATH . '/includes/components/widgets/class-lmb-product-in-stock-checker.php';
        include_once LMB_PATH . '/includes/components/widgets/class-lmb-filter-categories.php';
        include_once LMB_PATH . '/includes/components/widgets/class-lmb-filter-tag.php';
        // include_once LMB_PATH . '/includes/components/lmb-woocommerce.php';
        include_once LMB_PATH . '/includes/components/sliders/customizers/class-categorized-products-slider-customizer.php';
        include_once LMB_PATH . '/includes/components/sliders/class-popular-products-slider.php';
        include_once LMB_PATH . '/includes/components/sliders/class-categorized-products-slider.php';

        include_once LMB_PATH . '/includes/lmb-functions.php';
        include_once LMB_PATH . '/includes/lmb-template-functions.php';
        include_once LMB_PATH . '/includes/lmb-hooks.php';
    }

    private function set_components() {
        $this->query               = new LMB_Query();
        $this->cart                = new LMB_Cart();
        $this->checkout            = new LMB_Checkout();
        $this->my_account          = new LMB_My_Account();
        $this->initializer_factory = new LMB_Initializer_Factory();
        new Categorized_Products_Slider_Customizer();
        new LMB_Filter_Tag_Customizer();
    }

    public function init_widgets() {
        $this->initializer_factory->make('Product_In_Stock_Checker');
        $this->initializer_factory->make('Filter_Categories');
        $this->initializer_factory->make('Filter_Tag');
    }
}

function LMB() {
    return LMB_Theme::get_theme_object();
}

LMB();


add_filter( 'woocommerce_register_post_type_product', function( $options ) {
    $options['supports'][] = 'revisions';
    return $options;
}, 10, 1);

add_action( 'admin_enqueue_scripts', function( $hook_suffix ) {
//    if ( isset( $_GET['post'] ) && ( $wc_product = wc_get_product( $_GET['post'] ) ) instanceof WC_Product && $wc_product->post_type === 'product' ) {
                wp_enqueue_style( 'lmb_adm_style',  lmb_get_manifest_data_single('admcustom.css') );
                wp_enqueue_script( 'vendor', lmb_get_manifest_data_single('vendor.js'), array( 'wc-components', 'wc-navigation', 'wp-date', 'wp-html-entities', 'wp-keycodes', 'wp-i18n', 'moment', 'wc-admin-app' ), '1.0.0', true );
                wp_enqueue_script( 'lmb_woocommerce_customization', lmb_get_manifest_data_single('admcustom.js'),array( 'vendor' ), '1.0.0', true );
//    }
}, 10, 1 );

add_action( 'widgets_init', 'lmb_widget_initialization' );
function lmb_widget_initialization() {
    register_sidebar(array(
        'name' => __( 'Боковая колонка с фильтром продуктов', 'letmebuy' ),
        'id'   => 'product-filter',
        'description'   => __( 'Для продуктовых фильтров', 'letmebuy' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));
}

add_filter( 'woocommerce_price_filter_widget_min_amount', function( $min_price ) {
    if ( $min_price ) {
        return 0;
    }
    return $min_price;
} );


function lmb_get_widget_instance( $class_name ) {
    global $wp_widget_factory;

    if ( ! isset( $wp_widget_factory->widgets[ $class_name ] ) ) {
        return;
    }
    return $wp_widget_factory->widgets[ $class_name ];
}

/**
 * @param mixed[] $input function to sanitize user input
 * can accept array or string
 */
function lmb_sanitize_text_field( $input ) {
    if ( is_array( $input ) ) {
        return array_map( 'sanitize_text_field', $input );
    }

    return sanitize_text_field( $input );
}

add_action( 'after_setup_theme', function() {
    if ( $priority = has_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb' ) ) {
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', $priority );
        add_action( 'lmb_before_main_content_wrapper', 'woocommerce_breadcrumb' );
    }
});

add_filter( 'woocommerce_pagination_args', function( $args ) {
    if ( isset( $args['prev_text'] ) && !empty( $args['prev_text'] ) ) {
        $args['prev_text'] = "<span>" . $args['prev_text'] . "</span>";
    }
    if ( isset( $args['next_text'] ) && !empty( $args['next_text'] ) ) {
        $args['next_text'] = "<span>" . $args['next_text'] . "</span>";
    }
    return $args;
});


add_filter( 'woocommerce_single_product_image_thumbnail_html', function( $html, $attachment_id ) {
    global $product;
    $post_thumbnail_id = $product->get_image_id();
    if ( is_singular( 'product' ) && $post_thumbnail_id === $attachment_id ) {
        return "<div id=\"lmb-single-product__image-wrapper\" class=\"lmb-single-product__image-wrapper\">
                    {$html}
                </div>";
    }
    return $html;
}, 10, 2 ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped


add_filter( 'wp_get_attachment_image_attributes', function( $attr, $attachment, $size ) {
    if ( is_singular( 'product' ) && ! isset( $attr['id'] ) ) {
        $attr['id'] = 'img-' . $attachment->ID;
    }

    return $attr;
}, 10, 3 );


add_action( 'wp_ajax_lmb_get_attachment_html', function() {
    if ( isset( $_POST['attachment_id'] ) && !empty( $_POST['attachment_id'] ) ) {
        wp_send_json( array( 'markup' => lmb_get_gallery_image_html( sanitize_html_class( $_POST['attachment_id'] ), true ) ), 200 );
    }
} );

//check later start
add_filter( 'wp_get_attachment_image_attributes', function( $attr, $attachment, $size ) {
    if ( $size === 'woocommerce_single' ) {
        $attr['class'] = trim( $attr['class'] . ' objectf-cover' );
    }
    return $attr;
}, 10, 3);

//related to single-product page start
add_action('wp_loaded', function() {
    if ( false !== ( $priority = has_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt' ) ) ) {
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', $priority );
    }
    if ( false !== ( $priority = has_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating' ) ) ) {
        if( remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', $priority ) ) {
            add_action( 'woocommerce_review_meta', 'woocommerce_review_display_rating', 20 );
        }
    }

    if ( false !== ( $priority = has_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text' ) ) ) {
        if( remove_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', $priority ) ) {
            add_action( 'woocommerce_review_after_comment_text', 'woocommerce_review_display_comment_text' );
        }
    }

    if ( false !== ( $priority = has_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices' ) ) ) {
        if( remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', $priority ) ) {
            add_action( 'woocommerce_before_single_product', 'lmb_output_all_notices' );
        }
    }
    add_action( 'woocommerce_get_stock_html', function( $html, $product ) {
        if ( is_singular( 'product' ) ) {
            return '';
        }
        return $html;
    }, 10, 2 );

    add_filter('lmb_show_meta_single_product', '__return_false' );

    add_filter( 'woocommerce_product_tabs', function( $tabs ) {
        global $product;
        if ( !empty( $tabs ) && is_array( $tabs ) ) {
            foreach( $tabs as &$tab ) {
                if ( isset( $tab['title'] ) ) {
                    switch( $tab ) {
                        case $tab['title'] === 'Description':
                            $tab['title'] = __( 'Описание', 'letmebuy' );
                            break;
                        case $tab['title'] === 'Additional information':
                            $tab['title'] = __( 'Дополнительная информация', 'letmebuy' );
                            break;
                        case strpos( $tab['title'], 'Reviews' ) !== false:
                            $tab['title'] = sprintf( __( 'Отзывы (%d)', 'letmebuy' ), $product->get_review_count() );
                            break;
                    }
                }
            }
        }
        return $tabs;
    }, 11);

    add_filter( 'woocommerce_product_tabs', function( $tabs ) {
        global $product;
        if ( function_exists( 'acf' ) && false !== get_field_objects( $product->get_id() ) ) {
            if ( ! isset( $tabs['additional_information'] ) ) {
                $tabs['additional_information'] = array(
                    'title'    => __( 'Дополнительная информация', 'letmebuy' ),
                    'priority' => 20,
                    'callback' => 'lmb_product_additional_information_tab',
                );
            } else if ( isset( $tabs['additional_information']['callback'] )
                            && $tabs['additional_information']['callback'] !== 'lmb_product_additional_information_tab' ) {
                $tabs['additional_information']['callback'] = 'lmb_product_additional_information_tab';
            }
        }

        return $tabs;
    });

    add_filter( 'woocommerce_product_description_heading', function( $description ) {
        return __( 'Описание', 'letmebuy' );
    } );

    add_filter( 'woocommerce_product_review_comment_form_args', function( $comment_form ) {
        if ( !isset( $comment_form['class_submit'] ) ) {
            $comment_form['class_submit'] = 'lmb-button';
        } else {
            $comment_form['class_submit'] = $comment_form['class_submit'] . ' lmb-button lmb-button_regular';
        }
        return $comment_form;
    });
    add_action( 'woocommerce_single_product_summary', function( $arg, $template_path = '/includes/templates/single-product/benefits' ) {
        if ( file_exists( get_stylesheet_directory() . $template_path .'.php' ) ) {
            get_template_part( $template_path );
        }
    }, 35, 1 );
    add_action( 'woocommerce_product_review_comment_form_args', function( $defaults ) {
        $defaults['title_reply'] = have_comments() ? esc_html__( 'Добавить отзыв', 'letmebuy' ) : sprintf( esc_html__( 'Оставить первый отзыв для &ldquo;%s&rdquo;', 'letmebuy' ), get_the_title() );
        return $defaults;
    }, 10, 1 );
});

function lmb_product_additional_information_tab( $tab_name, $tab_value, $template_path = '/includes/templates/single-product/tabs/additional-info' ) {
    if ( file_exists( get_stylesheet_directory() . $template_path .'.php' ) ) {
        get_template_part( $template_path );
    }
}


add_action('wp_loaded', 'redefine_hooks' );

function redefine_hooks() {
    if ( false !== ( $priority = has_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices' ) ) ) {
        if ( remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices', $priority ) ) {
            add_action( 'woocommerce_before_cart', 'lmb_output_all_notices' );
        }
    }
}
function lmb_output_all_notices() {
    $notices = wc_print_notices( true );

    if ( ! $notices ) {
        return;
    }

    echo "
        <div class=\"col-12\">
            {$notices}
        </div>
        <div class=\"w-100 mb-3\"></div>";
}

function get_standart_page_wrapper() : array {
    $parts = array();
    $parts['wrapper_start'] = "<div class='container woocommerce'><div class='row'><div class='col-12'>";
    $parts['wrapper_end'] = "</div></div></div>";
    return $parts;
}
//code below updates the cart icon counter
add_filter( 'woocommerce_add_to_cart_fragments', function ( $fragments ) {
    $fragments['span#lmb-cart-counter'] = '<span class="header__cart_product-counter" id="lmb-cart-counter">' . WC()->cart->get_cart_contents_count() . '</span>';
    return $fragments;
} );

add_filter( 'woocommerce_cart_item_thumbnail', function( $img, $cart_item, $cart_item_key ) {
    $_product = $cart_item['data'];
    return $_product->get_image( 'woocommerce_thumbnail', array('class' => 'objectf-contain') );
}, 10, 3);

add_filter( 'wp_nav_menu_objects', function( $sorted_menu_items, $args ) {

    if ( empty( $args->menu_id ) ) {
        return $sorted_menu_items;
    }
    foreach ( $sorted_menu_items as &$item ) {
        $item->classes[] = "dropdown-item";
        $item->classes = array_filter( $item->classes );
    }
    unset( $item );

    return $sorted_menu_items;
}, 10, 2);
//
//class LMB_Script_Handler {
//    private $wp_script;
//    private $excluded_script_handles = array( 'jquery-core' );
//
//    public function __construct() {
//        add_action( 'wp_default_scripts', array( $this, 'get_wp_script_object' ) );
//        add_filter( 'script_loader_tag', array( $this, 'set_async_script' ), 10, 3 );
//    }
//
//    public function is_wp_script_loaded() {
//        if ( $this->wp_script instanceof WP_Scripts ) {
//            return true;
//        }
//        return false;
//    }
//
//    public function is_allowed_handle( $handle ) {
//        return !in_array( $handle, $this->excluded_script_handles );
//    }
//
//    public function get_wp_script_object( $wp_script ) {
//        if ( $wp_script instanceof WP_Scripts ) {
//            $this->wp_script = $wp_script;
//        }
//    }
//
//    public function set_async_script( $tag, $handle, $src ) {
//        if ( did_action( 'get_footer' ) || !$this->is_wp_script_loaded() || !$this->is_allowed_handle( $handle ) ) {
//            return $tag;
//        }
//
//        $type_attr = $this->get_class_private_property( $this->wp_script, 'type_attr' );
//        $obj = $this->wp_script->registered[ $handle ];
//        $cond_before = '';
//        $cond_after  = '';
//        $conditional = isset( $obj->extra['conditional'] ) ? $obj->extra['conditional'] : '';
//
//        if ( $conditional ) {
//            $cond_before = "<!--[if {$conditional}]>\n";
//            $cond_after  = "<![endif]-->\n";
//        }
//
//        $before_handle = $this->wp_script->print_inline_script( $handle, 'before', false );
//        $after_handle  = $this->wp_script->print_inline_script( $handle, 'after', false );
//
//        if ( $before_handle ) {
//            $before_handle = sprintf( "<script%s defer id='%s-js-before'>\n%s\n</script>\n", $type_attr, esc_attr( $handle ), $before_handle );
//        }
//
//        if ( $after_handle ) {
//            $after_handle = sprintf( "<script%s defer id='%s-js-after'>\n%s\n</script>\n", $type_attr, esc_attr( $handle ), $after_handle );
//        }
//
//        $translations = $this->wp_script->print_translations( $handle, false );
//        if ( $translations ) {
//            $translations = sprintf( "<script%s defer id='%s-js-translations'>\n%s\n</script>\n", $type_attr, esc_attr( $handle ), $translations );
//        }
//
//        $new_tag  = $translations . $cond_before . $before_handle;
//        $new_tag .= sprintf( "<script%s defer src='%s' id='%s-js'></script>\n", $type_attr, $src, esc_attr( $handle ) );
//        $new_tag .= $after_handle . $cond_after;
//
//        return $new_tag;
//    }
//
//    private function get_class_private_property( $object, $property_name ) {
//        $class = new ReflectionClass( get_class( $object ) );
//        $property = $class->getProperty( $property_name );
//        $property->setAccessible(true);
//        return $property->getValue( $object );
//    }
//}
//new LMB_Script_Handler();

//class Test_Class {
//    private $wp_styles;
//    private $main_page_styles = array('home-css', 'commons-css');
//
//    public function __construct() {
//        add_action( 'wp_default_styles', array( $this, 'load_wp_style' ) );
//        add_action( 'lmb_forming_tag_start', array( $this, 'maybe_concat_style' ) );
//        add_action( 'lmb_forming_tag_end', array( $this, 'unset_concat' ) );
//    }
//
//    public function load_wp_style( $wp_styles ) {
//        $this->wp_styles = $wp_styles;
//    }
//
//    private function is_wp_styles_loaded() {
//        if ( $this->wp_styles instanceof WP_Styles ) {
//            return true;
//        }
//        return false;
//    }
//
//    public function maybe_concat_style( $handle ) {
//        if ( !$this->is_wp_styles_loaded() || !in_array( $handle, $this->main_page_styles ) ) {
//            return $handle;
//        }
//        $this->wp_styles->do_concat = true;
//    }
//
//    public function unset_concat( $handle ) {
//        $this->wp_styles->do_concat = false;
//    }
//}

//new Test_Class();