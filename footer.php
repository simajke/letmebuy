
        <div class="w-100 mt-5"></div>
        <footer class="bg-lbrown py-5">
            <div class="container">
                <div class="row">
                    <div class="col-5 col-md-3 col-lg-3">
                        <h3 class="upper-case mb-4">Категории</h3>
                        <ul class="mb-0">
                            <?php
                                $categories = get_site_category_links(array('class' => 'h3'));
                                if ( !empty( $categories ) ) {
                                    foreach ( $categories as $category ) {
                                        echo '<li>';
                                        echo $category;
                                        echo '</li>';
                                    }
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="col-3 d-none d-md-block col-lg-2">
                        <h3 class="upper-case mb-4">Покупателю</h3>
                        <ul class="mb-0">
                            <li>
                                <a class="h3" href="#">Условия оплаты</a>
                            </li>
                            <li>
                                <a class="h3" href="#">Условия доставки</a>
                            </li>
                            <li>
                                <a class="h3" href="#">Обмен и возврат</a>
                            </li>
                            <li>
                                <a class="h3" href="#">Гарантия на товар</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-3 d-none d-md-block col-lg-2">
                        <h3 class="upper-case mb-4">О нас</h3>
                        <ul class="mb-0">
                            <li>
                                <a class="h3" href="#">О магазине</a>
                            </li>
                            <li>
                                <a class="h3" href="#">Обратная связь</a>
                            </li>
                            <li>
                                <a class="h3" href="#">Отзывы</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-5 col-md-3 col-lg-3 offset-2 offset-md-0 offset-lg-2">
                        <h3 class="upper-case mb-4">оставайтесь на связи</h3>
                        <ul class="mb-0">
                            <li>
                                <a class="h3" href="#">с 10:00 до 21:00</a>
                            </li>
                            <li>
                                <a class="h3" href="#">+7 (499) 999 99 99</a>
                            </li>
                            <li>
                                <a class="h3" href="#">mail</a>
<!--                                <a class="h3" href="#">example@gmail.com</a>-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>