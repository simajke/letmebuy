<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php wp_head() ?>
</head>
<body>
    <header class="mb-5 mb-lg-0">
        <div class="container">
            <!--   For desktops start   -->
            <div class="row align-items-center my-5 d-none d-md-flex">
                <div class="col-4 col-lg-3 d-flex align-items-center">
                    <div class="header__logo-wrapper">
                        <a href="<?php echo esc_url( home_url() ) ?>">
                            <img src="<?php echo lmb_get_manifest_data_single('img/logo.svg') ?>" alt="" class="header__logo">
                        </a>
                    </div>
                    <div class="header__name-wrapper d-flex flex-column">
                        <span class="header__name font-weight-bold">let me buy</span>
                        <span class="header__additional-text color-dark-brown-opacity">Качественно и выгодно</span>
                    </div>
                </div>
                <div class="col-5 col-lg-6">
                    <form class="header__search-form" action="/">
                        <label class="lmb-search-label" for="search">
                            <div class="header__search-wrapper position-absolute w-100 h-100" id="search"></div>
                            <input class="header__search"
                                type="text"
                                placeholder="Я хочу купить..."
                                autocomplete="off">
                        </label>
                    </form>
                </div>
                <div class="col-3 col-lg-3 d-flex justify-content-end">
                    <div class="header__phone-work_wrapper flex-column">
                        <span class="header__phone font-weight-bold">
                            8 (499) 999 99 99
                        </span>
                        <div class="header__work-time color-dark-brown-opacity">
                            <span>время работы: </span><span>с 10:00 до 21:00</span>
                        </div>
                    </div>
                </div>
            </div>
            <!--   For desktops end   -->
            <!--   For mobile devices start   -->
            <div class="row d-md-none mt-3">
                <div class="col-4">
                    <div class="header__work-time color-dark-brown-opacity d-flex justify-content-center flex-column h-100">
                        <span>время работы: </span><span>с 10:00 до 21:00</span>
                    </div>
                </div>
                <div class="col-4 d-flex align-items-center justify-content-center">
                    <div class="header__logo-wrapper">
                        <a href="<?php echo esc_url( home_url() ) ?>">
                            <img src="<?php echo lmb_get_manifest_data_single('img/logo.svg') ?>" alt="" class="header__logo">
                        </a>
                    </div>
                </div>
                <div class="col-4">
                    <div class="header__phone-work_wrapper d-flex flex-column align-items-end justify-content-center h-100">
                        <span class="header__phone font-weight-bold">
                            8 (499) 999 99 99
                        </span>
                    </div>
                </div>
            </div>
            <nav class="navbar my-3 navbar-light align-items-stretch justify-content-center d-md-none p-0">
                <div class="col-12 header__menu_main-section justify-content-start">
                    <button class="navbar-toggler bg-cyan" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <span class="d-none d-lg-block">все категории</span>
<!--                </div>-->
<!--                <div class="col-10">-->
                    <form class="header__search-form" action="/">
                        <label class="lmb-search-label" for="search">
                            <div class="header__search-wrapper position-absolute w-100 h-100" id="search"></div>
                            <input class="header__search"
                                   type="text"
                                   placeholder="Я хочу купить..."
                                   autocomplete="off">
                        </label>
                    </form>
                </div>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <?php
                    lmb_display_header_manu_categories();
                    ?>
                </div>
            </nav>
            <div class="row border-top-light-brown border-bottom-shadow d-md-none">
                <div class="col-12">
                    <div class="lmb-mobile-menu-wrapper">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Навигация по сайту</a>
                        <?php wp_nav_menu( array(
                            'menu'       => 'Main',
                            'container'  => '',
                            'items_wrap'           => '<div id="%1$s" class="%2$s">%3$s</div>',
                            'menu_id'    => 'lmb_mobile_menu',
                            'menu_class' => 'dropdown-menu'
                        ) ); ?>
                        <!--                        <div class="dropdown-menu">-->
<!--                            <a class="dropdown-item" href="#">Action</a>-->
<!--                            <a class="dropdown-item" href="#">Another action</a>-->
<!--                            <a class="dropdown-item" href="#">Something else here</a>-->
<!--                            <div class="dropdown-divider"></div>-->
<!--                            <a class="dropdown-item" href="#">Separated link</a>-->
<!--                        </div>-->
<!--                        <div class="nav-item dropdown">-->
<!--                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Навигация по сайту</a>-->
<!--                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 42px, 0px); top: 0px; left: 0px; will-change: transform;">-->
<!--                                <a class="dropdown-item" href="#">Action</a>-->
<!--                                <a class="dropdown-item" href="#">Another action</a>-->
<!--                                <a class="dropdown-item" href="#">Something else here</a>-->
<!--                                <a class="dropdown-item" href="#">Separated link</a>-->
<!--                                <div class="dropdown-divider"></div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <?php wp_nav_menu( array(
                            'menu'       => 'Main',
                            'menu_class' => 'dropdown-menu'
                        ) ); ?>
                    </div>
                </div>
            </div>
            <!--   For mobile devices end   -->
        </div>
        <div class="container-fluid bg-lbrown">
            <div class="row">
                <div class="container">
                    <!--   For desktops start   -->
                    <nav class="navbar navbar-light align-items-stretch justify-content-start d-none d-md-flex p-0">
                        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                        <div class="col-1 col-lg-3 header__menu_main-section ">
                            <button class="navbar-toggler bg-cyan p-2" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <span class="d-none d-lg-inline ml-3">все категории</span>
                        </div>
                        <div class="col-9 col-lg-7 d-flex align-items-center">
                            <?php wp_nav_menu( array(
                                'menu'       => 'Main',
                                'menu_class' => 'header__menu header__menu_collapsed d-flex justify-content-around m-0'
                            ) ); ?>
                        </div>
                        <div class="col-2 col-lg-2">
                            <div class="row h-100">
                                <div class="col-12 d-flex justify-content-end">
                                    <div class="header__liked-items_wrapper header__interactive-icon_wrapper d-flex align-items-center">
                                        <div class="header__liked-items position-absolute absolute-center">
                                        </div>
                                        <a class="position-absolute d-block h-100 w-100" href="#"></a>
                                    </div>
                                    <div class="header__cart_wrapper header__interactive-icon_wrapper d-flex align-items-center">
                                        <div class="header__cart position-absolute absolute-center">
                                        </div>
                                        <a class="header__cart-link position-absolute d-block h-100 w-100" href="<?php echo esc_url( wc_get_cart_url() ); ?>"><span class="header__cart_product-counter" id="lmb-cart-counter"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <?php lmb_display_header_manu_categories(); ?>
                        </div>
                    </nav>
                    <!--   For desktops end   -->
                </div>
            </div>
        </div>
    </header>