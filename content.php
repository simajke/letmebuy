<!-- Banner header -->
<section>
    <div class="container">
<!--        <div class="lmb-space__before-slick-load"></div>-->
        <div class="home__main-banner_context position-relative">
            <div class="home__main-banner_item position-relative">
                <div class="home__main-banner_wrapper">
                    <img class="home__main-banner" src="<?php echo lmb_get_manifest_data_single('img/hbanner.svg') ?>" alt="" class="img-fluid">
                </div>
                <div class="home__main-banner_text-area position-absolute absolute-center">
                    <div class="row h-100">
                        <div class="col-7 d-flex align-items-center">
                            <div class="home__main-banner_content-context ml-5">
                                <h1 class="font-xl-large font-md-large font-sm-large font-color-cyan mb-3">Робот-пылесос Xiaomi Mijia 1S</h1>
                                <div class="home__main-banner_itemtext-wrapper mb-4">
                                    <span class="h4">Еще больше комфорта и производительности</span>
                                </div>
                                <div class="home_lmb-button_wrapper">
                                    <button class="lmb-button lmb-button_large upper-case">
                                        подробнее
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-5"></div>
                    </div>
                </div>
            </div>
            <div class="home__main-banner_item position-relative">
                <div class="home__main-banner_wrapper">
                    <img class="home__main-banner" src="<?php echo lmb_get_manifest_data_single('img/hbanner.svg') ?>" alt="" class="img-fluid">
                </div>
                <div class="home__main-banner_text-area position-absolute absolute-center">
                    <div class="row h-100">
                        <div class="col-7 d-flex align-items-center">
                            <div class="home__main-banner_content-context ml-5">
                                <h1 class="font-xl-large font-md-large font-sm-large font-color-cyan mb-3">Робот-пылесос Xiaomi Mijia 1S</h1>
                                <div class="home__main-banner_itemtext-wrapper mb-4">
                                    <span class="h4">Еще больше комфорта и производительности</span>
                                </div>
                                <div class="home_lmb-button_wrapper">
                                    <button class="lmb-button lmb-button_large upper-case">
                                        подробнее
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-5"></div>
                    </div>
                </div>
            </div>
            <div class="home__main-banner_item position-relative">
                <div class="home__main-banner_wrapper">
                    <img class="home__main-banner" src="<?php echo lmb_get_manifest_data_single('img/hbanner.svg') ?>" alt="" class="img-fluid">
                </div>
                <div class="home__main-banner_text-area position-absolute absolute-center">
                    <div class="row h-100">
                        <div class="col-7 d-flex align-items-center">
                            <div class="home__main-banner_content-context ml-5">
                                <h1 class="font-xl-large font-md-large font-sm-large font-color-cyan mb-3">Робот-пылесос Xiaomi Mijia 1S</h1>
                                <div class="home__main-banner_itemtext-wrapper mb-4">
                                    <span class="h4">Еще больше комфорта и производительности</span>
                                </div>
                                <div class="home_lmb-button_wrapper">
                                    <button class="lmb-button lmb-button_large upper-case">
                                        подробнее
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner header end -->
<div class="w-100 mb-5"></div>
<!-- Benefits -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mb-5 mb-sm-0">
                <div class="row d-flex flex-column d-lg-flex flex-lg-row text-center">
                    <div class="col-12 col-lg-4 mb-2 mb-lg-0">
                        <div class="home-benefits__img-wrapper">
                            <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/delivery.svg') ?>" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="home-benefits__text-wrapper d-flex flex-column justify-content-center h-100">
                            <h3 class="home-benefits__headline">Доставка и оплата</h3>
                            <div class="home-benefits__text">Отправка по всей России почтой и курьером</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-5 mb-sm-0">
                <div class="row d-flex flex-column d-lg-flex flex-lg-row text-center">
                    <div class="col-12 col-lg-4 mb-2 mb-lg-0">
                        <div class="home-benefits__img-wrapper">
                            <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/quality.svg') ?>" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="home-benefits__text-wrapper d-flex flex-column justify-content-center h-100">
                            <h3 class="home-benefits__headline">Качество товаров</h3>
                            <div class="home-benefits__text">Наличие документации по всей продукции</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row d-flex flex-column d-lg-flex flex-lg-row text-center">
                    <div class="col-12 col-lg-4 mb-2 mb-lg-0">
                        <div class="home-benefits__img-wrapper">
                            <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/moneyback.svg') ?>" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="home-benefits__text-wrapper d-flex flex-column justify-content-center h-100">
                            <h3 class="home-benefits__headline">Возврат денег</h3>
                            <div class="home-benefits__text">В случае брака обмен или возврат денежных средств</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Benefits end -->
<div class="w-100 mb-5"></div>
<!-- Buyers choice -->
<?php
new Popular_Products_Slider( array(
    "best_selling" => true,
    "tag"          => "tech",
    "lmb_slider"   => true //musthave option
) );
?>
<!-- Buyers choice end -->
<div class="w-100 mb-5"></div>
<!-- Center banner -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/center-banner.svg') ?>" alt="">
            </div>
        </div>
    </div>
</section>
<!-- Center banner end -->
<div class="w-100 mb-5"></div>
<!-- Slider by brand -->
<?php
new Categorized_Products_Slider( array(
    "lmb_slider"   => true //musthave option
) );
?>
<!-- Slider by brand end -->
<div class="w-100 mb-5"></div>
<!-- Bottom banners -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="home-bbanners__img-wrapper">
                    <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/moza-banner.svg'); ?>" alt="">
                </div>
            </div>
            <div class="col-6">
                <div class="home-bbanners__img-wrapper">
                    <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/moza-banner.svg'); ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Bottom banners end -->