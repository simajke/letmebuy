<?php
function after_init_theme() {
    /**
     * Add default posts and comments RSS feed links to head.
     */
    add_theme_support( 'automatic-feed-links' );

    /** Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#Post_Thumbnails
     */
    add_theme_support( 'post-thumbnails' );

    /** Switch default core markup for search form, comment form, comments, galleries, captions and widgets
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'widgets',
        'style',
        'script',
    ) );

    /**
     * Declare support for selective refreshing of widgets.
     */
    add_theme_support( 'customize-selective-refresh-widgets' );


    add_theme_support(
        'woocommerce', apply_filters(
            'storefront_woocommerce_args', array(
                'single_image_width'    => 600,
                'thumbnail_image_width' => 250,
                'product_grid'          => array(
                    'default_columns' => 4,
                    'default_rows'    => 2,
                    'min_columns'     => 1,
                    'max_columns'     => 6,
                    'min_rows'        => 1,
                ),
            )
        )
    );

    add_filter( 'woocommerce_enqueue_styles', '__return_false' );
}

function lmb_enqueue_scripts_and_styles() {
    wp_enqueue_script( 'lmb-jquery', LMB_ASSETS_PATH . 'js/libraries/jquery.min.js', array('jquery-core'), '3.5.1' );
    wp_enqueue_script( 'lmb-popper', LMB_ASSETS_PATH . 'js/libraries/popper.min.js', array(), '1.16.1', true ); //'lmb-jquery'
    wp_enqueue_script( 'lmb-bootstrap', LMB_ASSETS_PATH . 'js/libraries/bootstrap.min.js', array('lmb-popper'), '4.5.0', true ); //'lmb-jquery',
    $vendor_dependencies = array( 'lmb-popper', 'lmb-bootstrap' ); //'lmb-jquery',
    if ( is_home() ) {
        $vendor_dependencies = array_merge( $vendor_dependencies, array('lmb-jquery-migrate', 'lmb-slick') );
    }
    wp_enqueue_script( 'vendor', lmb_get_manifest_data_single('vendor.js'), $vendor_dependencies, '1.0.0', true );
    wp_localize_script( 'vendor', 'lmb_ajax', array( 'url' => admin_url('admin-ajax.php'), 'assets_path' => LMB_ASSETS_PATH ) );

    if ( is_home() ) {
        wp_enqueue_script( 'lmb-jquery-migrate', LMB_ASSETS_PATH . 'js/libraries/jquery-migrate.min.js', array( 'lmb-popper', 'lmb-bootstrap'), '3.1.0',true ); //'lmb-jquery',
        wp_enqueue_script( 'lmb-slick', LMB_ASSETS_PATH . 'js/libraries/slick.min.js', array( 'lmb-popper', 'lmb-bootstrap', 'lmb-jquery-migrate'), '1.8.1',true ); //'lmb-jquery',
        wp_enqueue_script( 'home', lmb_get_manifest_data_single( 'home.js' ), array( 'vendor' ), '1.0.0',true );
        wp_enqueue_style( 'home-css', lmb_get_manifest_data_single( 'home.css' ) );
    }

    if ( is_shop() ) {
        wp_enqueue_script( 'shop', lmb_get_manifest_data_single('shop.js'), array('vendor'), '1.0.0',true );
        wp_enqueue_style( 'shop-css', lmb_get_manifest_data_single('shop.css') );
    }

    if ( is_cart() ) {
        wp_enqueue_script( 'cart', lmb_get_manifest_data_single('cart.js'), array('vendor', 'commons'), '1.0.0',true );
        wp_enqueue_style( 'cart-css', lmb_get_manifest_data_single('cart.css') );
    }

    if ( is_checkout() ) {
        wp_enqueue_script( 'checkout', lmb_get_manifest_data_single('checkout.js'), array('vendor'), '1.0.0',true );
        wp_enqueue_style( 'checkout-css', lmb_get_manifest_data_single('checkout.css') );
    }

    if ( is_product() ) {
        wp_enqueue_script( 'single-product', lmb_get_manifest_data_single('single-product.js'), array('vendor'), '1.0.0',true );
        wp_enqueue_style( 'single-product-css', lmb_get_manifest_data_single('single-product.css') );
    }

    if ( is_account_page() ) {
        wp_enqueue_style( 'myacc-css', lmb_get_manifest_data_single( 'myacc.css' ) );
    }

    if ( is_page() && !is_account_page() && !is_cart() && !is_checkout() ) {
        wp_enqueue_style( 'other-pages-css', lmb_get_manifest_data_single( 'other-pages.css' ) );
    }

    wp_enqueue_script( 'commons', lmb_get_manifest_data_single('commons.js'), array('vendor'), '1.0.0',true );
}

function ajax_lmb_get_search_result() {
    $search_word = sanitize_text_field( $_REQUEST['search_word'] );
    $args = array(
        's' => $search_word,
    );
    $query = new WP_Query();
    $query->parse_query( $args );
    relevanssi_do_query( $query );
    wp_send_json( array( 'markup' => get_search_result_html( $query->posts ) ), 200 );
}

function get_search_result_html( array $posts ) {
    $html_markup = "<div class=\"lmb-foundposts__wrapper\">";
    if ( ! empty( $posts ) ) {
        foreach( $posts as $p ) {
            $product = wc_get_product( $p->ID );
            $currency = get_woocommerce_currency_symbol();
            $post_image   = get_the_post_thumbnail(
                $p, 'post-thumbnail', array(
                                          'class' => 'img-fluid lmb-singlepost__img'
                                      )
            );
            $title        = get_the_title( $p );
            $html_markup .= "
            <div class=\"lmb-singlepost__wrapper\">
                <div class=\"lmb-singlepost__content-wrapper\">
                    <div class=\"lmb-singlepost__title-wrapper\">
                        <h3 class=\"mb-2\">{$title}</h3>
                    </div>
                    <div class=\"lmb-singlepost__description-wrapper\">
                        {$product->get_price()} {$currency}
                    </div>
                </div>
                <div class=\"lmb-singlepost__img-wrapper text-right\">
                    {$post_image}
                </div>
            </div>
            ";
        }
    } else {
        $html_markup .= "
                <span class=\"lmb-noresult\">
                    No results. Please try again
                </span>";
    }
    $html_markup .= "</div>";

    return $html_markup;
}

function lmb_register_menus() {
    register_nav_menu('new-menu',__( 'New Menu' ));
}

function lmb_get_manifest_data() {
    $manifest = file_get_contents( LMB_MANIFEST_PATH );
    $assets = json_decode( $manifest, true );
    if ( ! empty( $assets ) ) {
        return $assets;
    }
    return false;
}

function lmb_get_manifest_data_single( $key ) {
    if ( ! empty( $assets = lmb_get_manifest_data() ) && isset( $assets[ $key ] ) ) {
        $sss = LMB_ASSETS_PATH . $assets[ $key ];
        return LMB_ASSETS_PATH . $assets[ $key ];
    }
    return false;
}

/**
 *  The function below is equivalent woocommerce_subcategory_thumbnail( $term ).
 *  The main difference that we outuput terms' images without
 *  width and height attributes
*/
function lmb_subcategory_thumbnail( $c, $args = array() ) {
    $small_thumbnail_size = apply_filters( 'lmb_subcategory_archive_thumbnail_size', 'woocommerce_single' );
    $thumbnail_id = get_term_meta( $c->term_id, 'thumbnail_id', true );
    if ( $thumbnail_id ) {
        $image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
        $image        = $image[0];
        $image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
        $image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
    } else {
        $image        = wc_placeholder_img_src();
        $image_srcset = false;
        $image_sizes  = false;
    }

    if ( ! empty( $args ) &&  isset( $args['class'] ) ) {
        if ( is_array( $args['class'] ) ) {
            $class = "class='" . implode( " ", array_map( 'esc_attr', $args['class'] ) ) . "'";
        } else if ( is_string( $args['class'] ) && ! empty( trim( $args['class'] ) ) )  {
            $class = "class='" . esc_attr( $args['class'] ) . "'";
        }
    } else {
        $class = "";
    }

    if ( $image_srcset && $image_sizes ) {
    ?>
        <img <?php echo $class ?> src="<?php echo $image ?>" alt="<?php echo esc_attr( $c->name ) ?>" srcset="<?php echo esc_attr( $image_srcset ) ?>" sizes="<?php echo esc_attr( $image_sizes ) ?>">
    <?php
    } else {
    ?>
        <img <?php echo $class ?> src="<?php echo $image ?>" alt="<?php echo esc_attr( $c->name ) ?>">
    <?php
    }
}

function num_decline( $number, $titles, $param2 = '', $param3 = '', $with_number = true ){

	if( $param2 )
		$titles = [ $titles, $param2, $param3 ];

	if( is_string($titles) )
		$titles = preg_split( '/, */', $titles );

	if( empty($titles[2]) )
		$titles[2] = $titles[1]; // когда указано 2 элемента

	$cases = [ 2, 0, 1, 1, 1, 2 ];

	$intnum = abs( intval( strip_tags( $number ) ) );
    $result_title = $titles[ ($intnum % 100 > 4 && $intnum % 100 < 20) ? 2 : $cases[min($intnum % 10, 5)] ];
    if ( ! $with_number ) {
        return $result_title;
    }
	return "$number ". $result_title;
}
/**
 * @var WC_Product|WP_Comment Gets rating as html for objects
 */
function lmb_get_html_star_rating( $entity, $rating = '' ) {
    if ( ! wc_review_ratings_enabled() ) {
        return;
    }

    if ( !empty( $rating ) && !is_int( $rating ) ) {
        return;
    }

    if ( $entity instanceof WC_Product  || $entity instanceof WP_Comment ) {
        // if ( $entity instanceof WC_Product && $entity->get_id() === 72 ) {
        //     $stop = 1;
        // }
        $html_star_rating = '<div class="main-slick__stars-wrapper">';

        if ( $entity instanceof WC_Product ) {
            $rating = $entity->get_average_rating();
        }

        if ( $entity instanceof WP_Comment && empty( $rating ) ) {
            $rating = intval( get_comment_meta( $entity->comment_ID, 'rating', true ) ) ;
        }

        if ( $rating && ( $remainder = fmod($rating, 1) ) > 0 ) {
            $gradient_percentage = $remainder * 100;
        }

        for ( $rating_counter = 0; $rating_counter <= 4; $rating_counter++ ) {
            if ( !empty( $remainder ) && floatval( $rating ) - floatval( $rating_counter ) == $remainder ) {
                $html_star_rating .= lmb_get_svg_star( $gradient_percentage );
                continue;
            }

            if ( $rating_counter < $rating ) {
                $html_star_rating .= lmb_get_svg_star();
            } else {
                $html_star_rating .= lmb_get_svg_star( '', 'none');
            }
        }

        $html_star_rating .= "</div>";

        return $html_star_rating;
    }

    return;
}

function lmb_get_svg_star( $gradient_percentage = '', $color = '#0EB5C5' ) {
    $svg = '
    <svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg">';
    if ( ! empty( $gradient_percentage ) ) {
        $gradient_id = "myGradient";
        $second_color_percentage = $gradient_percentage + 1;
        $svg .= '
        <defs>
            <linearGradient id="'. $gradient_id . '">
                <stop offset="' . $gradient_percentage .  '%"  stop-color="' . $color . '" />
                <stop offset="' . $second_color_percentage .  '%" stop-color="transparent" />
            </linearGradient>
        </defs>';
        $color = 'url(\'#' . $gradient_id . '\')';
    }
    $svg .='<path
            d="M5.99833 0.787807C6.26881 -0.0364331 7.4348 -0.0364316 7.70528 0.787809L8.61174 3.5501C8.73277 3.91892 9.07703 4.16828 9.46521 4.16828H12.3795C13.2518 4.16828 13.6122 5.2863 12.9041 5.79571L10.5617 7.48073C10.2438 7.70946 10.1107 8.11784 10.2328 8.48999L11.1311 11.2274C11.4022 12.0534 10.4588 12.7443 9.75307 12.2367L7.37636 10.5269C7.06298 10.3015 6.64062 10.3015 6.32725 10.5269L3.95053 12.2367C3.24482 12.7443 2.30144 12.0534 2.5725 11.2274L3.47081 8.48998C3.59293 8.11784 3.45984 7.70946 3.14189 7.48073L0.799544 5.79571C0.0914214 5.2863 0.451784 4.16828 1.3241 4.16828H4.23839C4.62657 4.16828 4.97083 3.91892 5.09186 3.5501L5.99833 0.787807Z"
            fill="' . $color . '"
            stroke="black"
        />
        </svg>';
    return $svg;
}

function lmb_get_gallery_image_html( $attachment_id, $main_image = false ) {
	$flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
	$gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
	$thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( $gallery_thumbnail['width'], $gallery_thumbnail['height'] ) );
	$image_size        = apply_filters( 'woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_single' : $thumbnail_size );
	$full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
	$thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
	$full_src          = wp_get_attachment_image_src( $attachment_id, $full_size );
	$alt_text          = trim( wp_strip_all_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) );
	$image             = wp_get_attachment_image(
		$attachment_id,
		$image_size,
		false,
		apply_filters(
			'woocommerce_gallery_image_html_attachment_image_params',
			array(
				'title'                   => _wp_specialchars( get_post_field( 'post_title', $attachment_id ), ENT_QUOTES, 'UTF-8', true ),
				'data-caption'            => _wp_specialchars( get_post_field( 'post_excerpt', $attachment_id ), ENT_QUOTES, 'UTF-8', true ),
				'data-src'                => esc_url( $full_src[0] ),
				'data-large_image'        => esc_url( $full_src[0] ),
				'data-large_image_width'  => esc_attr( $full_src[1] ),
				'data-large_image_height' => esc_attr( $full_src[2] ),
				'class'                   => esc_attr( $main_image ? 'wp-post-image' : '' ),
			),
			$attachment_id,
			$image_size,
			$main_image
		)
	);

    $wrapper_class = $main_image ? 'lmb-product-gallery__main-wrapper' : 'lmb-product-gallery__attachments-wrapper';

	return '<div data-thumb="' . esc_url( $thumbnail_src[0] ) . '" data-thumb-alt="' . esc_attr( $alt_text ) . '" class="woocommerce-product-gallery__image ' . $wrapper_class . '"><a href="' . esc_url( $full_src[0] ) . '">' . $image . '</a></div>';
}

if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	/**
	 * Returns true if WooCommerce plugin is activated
	 *
	 * @return bool
	 */
	function is_woocommerce_activated() {
		return class_exists( 'woocommerce' );
	}
}

function init_lmb_styles() {
    global $wp_styles;

    if ( ! ( $wp_styles instanceof WP_Styles ) ) {
        $wp_styles = new LMB_Styles();
    }
}

function lmb_display_header_manu_categories() {
    $categories = get_site_category_links(array('class' => 'nav-item nav-link'));
    if ( ! empty( $categories ) ) {
        echo '<div class="navbar-nav">';
            foreach( $categories as $category ) {
                echo $category;
            }
        echo '</div>';
    }
}
function get_site_category_links( array $options ) {
    $categories = get_terms( array(
        'taxonomy'               => 'product_cat',
        'fields'                 => 'all',
        'orderby'                => 'name',
        'order'                  => 'ASC',
        'exclude'                => ( !isset( $options['exclude'] ) || empty( $options['exclude'] ) ) ? array( 15 ) : ( array ) $options['exclude']
    ));

    if ( empty( $categories ) ) {
        return array();
    }

    if ( empty( $options['base_path'] ) ) {
        $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
    } else {
        $shop_page_url = $base_path;
    }

    $links = array_map( function( $value ) use ( $shop_page_url, $options ) {
        $link = "<a";

        if ( isset( $options['class'] ) ) {
            $link .= " class='{$options['class']}'";
        }

        $link .= " href='" . esc_url_raw( $shop_page_url . '?category=' . $value->term_id ) . "'>";
        $link .= $value->name;
        $link .= '</a>';

        return $link;
    }, $categories );

    return $links;

}