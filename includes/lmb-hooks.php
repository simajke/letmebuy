<?php
add_action( 'wp_enqueue_scripts', 'lmb_enqueue_scripts_and_styles', 11, 1 );
add_action( 'init', 'lmb_register_menus' );
add_action( 'after_setup_theme', 'after_init_theme' );
// add_action( 'init', 'lmb_register_widgets_scripts_and_styles', 10, 1 );

if ( function_exists( 'is_woocommerce_activated' ) && is_woocommerce_activated() ) {
//    add_action( 'init', 'init_lmb_styles', 0 );
    add_action( 'widgets_init', array( $this, 'init_widgets' ) );
}
// AJAX
add_action('wp_ajax_categorized_slider', array( 'Categorized_Products_Slider', 'ajax_categorized_slider' ) );
add_action('wp_ajax_nopriv_categorized_slider', array( 'Categorized_Products_Slider', 'ajax_categorized_slider' ) );
add_action('wp_ajax_lmb_get_search_result', 'ajax_lmb_get_search_result');
add_action('wp_ajax_nopriv_lmb_get_search_result', 'ajax_lmb_get_search_result');
/* slider-template.php actions start  */
add_action('lmb_slider_template_footer_start', 'lmb_slider_template_footer_start', 10, 1 );
add_action('lmb_slider_template_footer_end', 'lmb_slider_template_footer_end', 10, 1 );
add_action('lmb_slider_template_footer_price', 'lmb_slider_template_footer_price', 10, 1 );
add_action( 'init', 'init_woocommerce_hooks_for_lmb_theme', 10, 1 );
/* slider-template.php actions end  */

add_action( 'wp_footer', 'lmb_set_modal_window', 10, 1 );