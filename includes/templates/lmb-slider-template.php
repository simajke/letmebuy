<?php
defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="main-slick__item-wrapper">
    <?php include locate_template("includes/templates/lmb-product-content.php") ?>
</div>