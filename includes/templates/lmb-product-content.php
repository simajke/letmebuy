<?php
defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$url = get_permalink( $product->get_id() );
?>
<div class="main-slick__inner_wrapper">
    <div class="main-slick__header d-flex justify-content-between">
        <?php
            echo lmb_get_html_star_rating( $product );
        ?>
        <div class="main-slick__reviews">
            <span class="color-dark-brown-opacity-half"><?php echo num_decline( $product->get_review_count(), array('отзыв', 'отзыва', 'отзывов') ) ?></span>
        </div>
    </div>
    <div class="main-slick__img-wrapper_context">
        <div class="main-slick__img-wrapper mb-3">
            <a href="<?php echo $url; ?>">
                <?php
                    the_post_thumbnail('post-thumbnail', array(
                        'class' => 'img-fluid'
                    ));
                ?>
            </a>
        </div>
    </div>
    <div class="main-slick__headline-wrapper">
        <a href="<?php echo $url; ?>" class="main-slick__headline h4">
            <?php the_title(); ?>
        </a>
    </div>
    <?php do_action('lmb_slider_template_footer_start') ?>

    <?php do_action('lmb_slider_template_footer_price') ?>

    <?php do_action('lmb_slider_template_footer_add_to_cart') ?>

    <?php do_action('lmb_slider_template_footer_end'); ?>
</div>