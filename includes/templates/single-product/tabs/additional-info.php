<?php
defined( 'ABSPATH' ) || exit;

global $product;
$product_fields = get_field_objects( $product->get_id() );
?>

<div class="lmb-tabs__main-wrapper">
    <div class="lmb-tabs__headline-weapper">
        <h2>
            <?php apply_filters( 'lmb_additional_info_label', _e( 'Общие характеристики', 'letmebuy' ) ) ?>
        </h2>
    </div>
    <?php if ( is_array( $product_fields ) && !empty( $product_fields ) ) { ?>
    <div class="lmb-tabs__additionalInfo-wrapper">
        <?php foreach( $product_fields as $field ) { ?>
            <div class="lmb-tabs__additionalInfo-row">
                <div class="lmb-tabs__additionalInfo-label">
                    <?php echo $field['label']; ?>
                </div>
                <div class="lmb-tabs__additionalInfo-space"></div>
                <div class="lmb-tabs__additionalInfo-value">
                    <?php
                    echo $field['value'];
                    if ( isset( $field['append'] ) && $field['append'] ) {
                        echo ' ' . $field['append'];
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php } ?>
</div>