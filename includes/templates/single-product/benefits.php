<div class="container px-0">
    <div class="row">
        <div class="col-12 mb-2">
            <div class="lmb-single__benefits-wrapper">
                <div class="lmb-single__benefits-image">
                    <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/delivery.svg') ?>" alt="">
                </div>
                <div class="lmb-single__benefits-textWrapper">
                    <div class="lmb-single__benefits-headline">
                        Доставка
                    </div>
                    <div class="lmb-single__benefits-text">
                        В понедельник, 8 июня - 300р
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mb-2">
            <div class="lmb-single__benefits-wrapper">
                <div class="lmb-single__benefits-image">
                    <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/quality.svg') ?>" alt="">
                </div>
                <div class="lmb-single__benefits-textWrapper">
                    <div class="lmb-single__benefits-headline">
                        Самовывоз
                    </div>
                    <div class="lmb-single__benefits-text">
                        В понедельник, 8 июня - 300р
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="lmb-single__benefits-wrapper">
                <div class="lmb-single__benefits-image">
                    <img class="img-fluid" src="<?php echo lmb_get_manifest_data_single('img/moneyback.svg') ?>" alt="">
                </div>
                <div class="lmb-single__benefits-textWrapper">
                    <div class="lmb-single__benefits-headline">
                        Оплата
                    </div>
                    <div class="lmb-single__benefits-text">
                        В понедельник, 8 июня - 300р
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>