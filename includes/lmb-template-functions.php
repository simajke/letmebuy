<?php
function init_woocommerce_hooks_for_lmb_theme() {
    if ( function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
        add_action( 'lmb_slider_template_footer_add_to_cart', function() {
            echo "<div class='lmb-button__wrapper d-flex justify-content-between align-items-center'>";
            woocommerce_template_loop_add_to_cart();
            echo "</div>";
        } , 10, 1 );

        add_filter( 'woocommerce_loop_add_to_cart_args', function( $args, $product ) {
            $lmb_defaults = array(
                'lmb-button',
                'lmb-button__add-to-cart',
                'lmb-button_regular',
                'd-inline-flex',
                'align-items-center'
            );
            $args['class'] = implode(
                ' ',
                array_filter(
                    wp_parse_args(
                        array(
                            'button',
                            'product_type_' . $product->get_type(),
                            $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                            $product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'ajax_add_to_cart' : '',
                        ),
                        $lmb_defaults
                    )
                )
            );
            return $args;
        }, 10, 2 );
    }
}
/* slider-template.php functions start  */
function lmb_slider_template_footer_start() {
    echo "<div class='main-slick__footer'>";
}

function lmb_slider_template_footer_end() {
    echo "</div>";
}

function lmb_slider_template_footer_price() {
    global $product;
    $template = "<div class='main-slick__price-wrapper'>
    <span class='main-slick__price h4 mb-0 font-weight-bold'>". wc_price( $product->get_price() ) . "</span>
    </div>";
    echo $template;
}

/* slider-template.php functions end  */

function lmb_set_modal_window() {
    get_template_part( 'includes/components/lmb-modal' );
}