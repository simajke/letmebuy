<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LMBWoocommerce {
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'setup' ) );
	}

	public function setup() {
		add_theme_support(
			'woocommerce', apply_filters(
				'storefront_woocommerce_args', array(
					'single_image_width'    => 600,
					'thumbnail_image_width' => 250,
					'product_grid'          => array(
						'default_columns' => 4,
						'default_rows'    => 2,
						'min_columns'     => 1,
						'max_columns'     => 6,
						'min_rows'        => 1,
					),
				)
			)
		);

		add_filter( 'woocommerce_enqueue_styles', '__return_false' );
	}

	/**
	 * Query WooCommerce Extension Activation.
	 *
	 * @param string $extension Extension class name.
	 * @return boolean
	 */
	public function is_extension_activated( $extension ) {
		return class_exists( $extension ) ? true : false;
	}
}

//new LMBWoocommerce();