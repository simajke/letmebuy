<?php
defined( 'ABSPATH' ) || exit;
/**
 * Enable images on taxonomy 'product_tag'.
 */
class LMB_Tag_Taxonomy_Customizer {

    private static $instance = null;

    public static function get_instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action( 'init', array( $this, 'set_term_image' ), 10, 1 );
    }

    public function set_term_image() {
        $admin_taxonomies =  WC_Admin_Taxonomies::get_instance();
        add_filter( 'manage_edit-product_tag_columns', array( $admin_taxonomies, 'product_cat_columns' ) );
        add_filter( 'manage_product_tag_custom_column', array( $admin_taxonomies, 'product_cat_column' ), 10, 3 );
        add_action( 'product_tag_add_form_fields', array( $this, 'add_term_fields' ) );
        add_action( 'product_tag_edit_form_fields', array( $this, 'edit_term_fields' ), 12 );
    }

    public function create_and_save_term( $term_id, $tt_id = '', $taxonomy = '' ) {

        if ( isset( $_POST['display_type'] ) && 'product_tag' === $taxonomy ) { // WPCS: CSRF ok, input var ok.
            update_term_meta( $term_id, 'display_type', esc_attr( $_POST['display_type'] ) ); // WPCS: CSRF ok, sanitization ok, input var ok.
        }
        if ( isset( $_POST['product_tag_thumbnail_id'] ) && 'product_tag' === $taxonomy ) { // WPCS: CSRF ok, input var ok.
            update_term_meta( $term_id, 'thumbnail_id', absint( $_POST['product_tag_thumbnail_id'] ) ); // WPCS: CSRF ok, input var ok.
        }
    }

    public function add_term_fields() {
        ?>
        <div class="form-field term-display-type-wrap">
            <label for="display_type"><?php esc_html_e( 'Display type', 'woocommerce' ); ?></label>
            <select id="display_type" name="display_type" class="postform">
                <option value=""><?php esc_html_e( 'Default', 'woocommerce' ); ?></option>
                <option value="products"><?php esc_html_e( 'Products', 'woocommerce' ); ?></option>
                <option value="subcategories"><?php esc_html_e( 'Subcategories', 'woocommerce' ); ?></option>
                <option value="both"><?php esc_html_e( 'Both', 'woocommerce' ); ?></option>
            </select>
        </div>
        <div class="form-field term-thumbnail-wrap">
            <label><?php esc_html_e( 'Thumbnail', 'woocommerce' ); ?></label>
            <div id="product_tag_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( wc_placeholder_img_src() ); ?>" width="60px" height="60px" /></div>
            <div style="line-height: 60px;">
                <input type="hidden" id="product_tag_thumbnail_id" name="product_tag_thumbnail_id" />
                <button type="button" class="upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'woocommerce' ); ?></button>
                <button type="button" class="remove_image_button button"><?php esc_html_e( 'Remove image', 'woocommerce' ); ?></button>
            </div>
            <script type="text/javascript">

                // Only show the "remove image" button when needed
                if ( ! jQuery( '#product_tag_thumbnail_id' ).val() ) {
                    jQuery( '.remove_image_button' ).hide();
                }

                // Uploading files
                var file_frame;

                jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        file_frame.open();
                        return;
                    }

                    // Create the media frame.
                    file_frame = wp.media.frames.downloadable_file = wp.media({
                        title: '<?php esc_html_e( 'Choose an image', 'woocommerce' ); ?>',
                        button: {
                            text: '<?php esc_html_e( 'Use image', 'woocommerce' ); ?>'
                        },
                        multiple: false
                    });

                    // When an image is selected, run a callback.
                    file_frame.on( 'select', function() {
                        var attachment           = file_frame.state().get( 'selection' ).first().toJSON();
                        var attachment_thumbnail = attachment.sizes.thumbnail || attachment.sizes.full;

                        jQuery( '#product_tag_thumbnail_id' ).val( attachment.id );
                        jQuery( '#product_tag_thumbnail' ).find( 'img' ).attr( 'src', attachment_thumbnail.url );
                        jQuery( '.remove_image_button' ).show();
                    });

                    // Finally, open the modal.
                    file_frame.open();
                });

                jQuery( document ).on( 'click', '.remove_image_button', function() {
                    jQuery( '#product_tag_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo esc_js( wc_placeholder_img_src() ); ?>' );
                    jQuery( '#product_tag_thumbnail_id' ).val( '' );
                    jQuery( '.remove_image_button' ).hide();
                    return false;
                });

                jQuery( document ).ajaxComplete( function( event, request, options ) {
                    if ( request && 4 === request.readyState && 200 === request.status
                        && options.data && 0 <= options.data.indexOf( 'action=add-tag' ) ) {

                        var res = wpAjax.parseAjaxResponse( request.responseXML, 'ajax-response' );
                        if ( ! res || res.errors ) {
                            return;
                        }
                        // Clear Thumbnail fields on submit
                        jQuery( '#product_tag_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo esc_js( wc_placeholder_img_src() ); ?>' );
                        jQuery( '#product_tag_thumbnail_id' ).val( '' );
                        jQuery( '.remove_image_button' ).hide();
                        // Clear Display type field on submit
                        jQuery( '#display_type' ).val( '' );
                        return;
                    }
                } );

            </script>
            <div class="clear"></div>
        </div>
        <?php
    }

    public function edit_term_fields( $term ) {

        $display_type = get_term_meta( $term->term_id, 'display_type', true );
        $thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );

        if ( $thumbnail_id ) {
            $image = wp_get_attachment_thumb_url( $thumbnail_id );
        } else {
            $image = wc_placeholder_img_src();
        }
        ?>
        <tr class="form-field term-display-type-wrap">
            <th scope="row" valign="top"><label><?php esc_html_e( 'Display type', 'woocommerce' ); ?></label></th>
            <td>
                <select id="display_type" name="display_type" class="postform">
                    <option value="" <?php selected( '', $display_type ); ?>><?php esc_html_e( 'Default', 'woocommerce' ); ?></option>
                    <option value="products" <?php selected( 'products', $display_type ); ?>><?php esc_html_e( 'Products', 'woocommerce' ); ?></option>
                    <option value="subcategories" <?php selected( 'subcategories', $display_type ); ?>><?php esc_html_e( 'Subcategories', 'woocommerce' ); ?></option>
                    <option value="both" <?php selected( 'both', $display_type ); ?>><?php esc_html_e( 'Both', 'woocommerce' ); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field term-thumbnail-wrap">
            <th scope="row" valign="top"><label><?php esc_html_e( 'Thumbnail', 'woocommerce' ); ?></label></th>
            <td>
                <div id="product_tag_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" /></div>
                <div style="line-height: 60px;">
                    <input type="hidden" id="product_tag_thumbnail_id" name="product_tag_thumbnail_id" value="<?php echo esc_attr( $thumbnail_id ); ?>" />
                    <button type="button" class="upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'woocommerce' ); ?></button>
                    <button type="button" class="remove_image_button button"><?php esc_html_e( 'Remove image', 'woocommerce' ); ?></button>
                </div>
                <script type="text/javascript">

                    // Only show the "remove image" button when needed
                    if ( '0' === jQuery( '#product_tag_thumbnail_id' ).val() ) {
                        jQuery( '.remove_image_button' ).hide();
                    }

                    // Uploading files
                    var file_frame;

                    jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                        event.preventDefault();

                        // If the media frame already exists, reopen it.
                        if ( file_frame ) {
                            file_frame.open();
                            return;
                        }

                        // Create the media frame.
                        file_frame = wp.media.frames.downloadable_file = wp.media({
                            title: '<?php esc_html_e( 'Choose an image', 'woocommerce' ); ?>',
                            button: {
                                text: '<?php esc_html_e( 'Use image', 'woocommerce' ); ?>'
                            },
                            multiple: false
                        });

                        // When an image is selected, run a callback.
                        file_frame.on( 'select', function() {
                            var attachment           = file_frame.state().get( 'selection' ).first().toJSON();
                            var attachment_thumbnail = attachment.sizes.thumbnail || attachment.sizes.full;

                            jQuery( '#product_tag_thumbnail_id' ).val( attachment.id );
                            jQuery( '#product_tag_thumbnail' ).find( 'img' ).attr( 'src', attachment_thumbnail.url );
                            jQuery( '.remove_image_button' ).show();
                        });

                        // Finally, open the modal.
                        file_frame.open();
                    });

                    jQuery( document ).on( 'click', '.remove_image_button', function() {
                        jQuery( '#product_tag_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo esc_js( wc_placeholder_img_src() ); ?>' );
                        jQuery( '#product_tag_thumbnail_id' ).val( '' );
                        jQuery( '.remove_image_button' ).hide();
                        return false;
                    });

                </script>
                <div class="clear"></div>
            </td>
        </tr>
        <?php
    }

}
LMB_Tag_Taxonomy_Customizer::get_instance();