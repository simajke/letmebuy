<?php
defined( 'ABSPATH' ) || exit;

class LMB_Checkout {
    const WOOCOMMERCE_SHORTCODE_TAG = 'woocommerce_checkout';

    public function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action('wp_loaded', array( $this, 'set_checkout_shortcode' ) );
        add_filter( 'woocommerce_checkout_fields', array( $this, 'set_user_comments_textarea' ) );
    }

    public function set_checkout_shortcode() {
        if ( function_exists( 'is_woocommerce_activated' ) && is_woocommerce_activated() ) {
            if ( shortcode_exists( self::WOOCOMMERCE_SHORTCODE_TAG ) ) {
                remove_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG );
            }
            add_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG, 'LMB_Checkout::checkout' );
        }
    }

    public static function checkout() {
        extract( get_standart_page_wrapper() );
		return WC_Shortcodes::shortcode_wrapper( array( 'WC_Shortcode_Checkout', 'output' ), array(), array( 'before' => $wrapper_start, 'after' => $wrapper_end ) );
    }

    public function set_user_comments_textarea( $fields ) {
        if ( isset( $fields['order']['order_comments'] ) ) {
            if ( ! isset( $fields['order']['order_comments']['custom_attributes'] ) ) {
                $fields['order']['order_comments']['custom_attributes'] = array(
                    'rows' => '',
                    'cols' => ''
                );
            }
            $fields['order']['order_comments']['custom_attributes']['rows'] = 5;
            $fields['order']['order_comments']['custom_attributes']['cols'] = 60;
        }
        return $fields;
    }

}

