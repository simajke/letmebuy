<?php
defined( 'ABSPATH' ) || exit;

class LMB_Cart {
    const WOOCOMMERCE_SHORTCODE_TAG = 'woocommerce_cart';

    public function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action('wp_loaded', array( $this, 'set_cart_shortcode' ) );
    }

    public function set_cart_shortcode() {
        if ( function_exists( 'is_woocommerce_activated' ) && is_woocommerce_activated() ) {
            if ( shortcode_exists( self::WOOCOMMERCE_SHORTCODE_TAG ) ) {
                remove_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG );
            }
            add_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG, 'LMB_Cart::cart' );
        }
    }

    public static function cart() {
        extract( get_standart_page_wrapper() );
		return is_null( WC()->cart ) ? '' : WC_Shortcodes::shortcode_wrapper( array( 'WC_Shortcode_Cart', 'output' ), array(), array( 'before' => $wrapper_start, 'after' => $wrapper_end ) );
    }
}