<?php
class LMB_My_Account {

    const WOOCOMMERCE_SHORTCODE_TAG = 'woocommerce_my_account';

    public function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action('wp_loaded', array( $this, 'set_myacc_shortcode' ) );
        add_filter( 'woocommerce_account_menu_items', array( $this, 'set_menu_elements' ), 10, 2);
        add_filter( 'lmb_acoount_formatted_adress', array( $this, 'format_address' ) );
    }

    public function set_myacc_shortcode() {
        if ( function_exists( 'is_woocommerce_activated' ) && is_woocommerce_activated() ) {
            if ( shortcode_exists( self::WOOCOMMERCE_SHORTCODE_TAG ) ) {
                remove_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG );
            }
            add_shortcode( self::WOOCOMMERCE_SHORTCODE_TAG, 'LMB_My_Account::myaccount' );
        }
    }

    public static function myaccount() {
        extract( get_standart_page_wrapper() );
		return WC_Shortcodes::shortcode_wrapper( array( 'WC_Shortcode_My_Account', 'output' ), array(), array( 'before' => $wrapper_start, 'after' => $wrapper_end ) );
    }

    public function set_menu_elements( $items, $endpoints ) {
        //, $items['edit-address']
        unset( $items['downloads'], $items['payment-methods'] );
        return $items;
    }


    /**
     * Replace breaking lines <br> to <div> to apply styles
     *
     * @param string $foramated_address Fully formated address
     * @return string $formated_address
     */
    public function format_address( $foramated_address ) {
        return '<div>' . implode( '</div><div>', explode( '<br/>', $foramated_address ) ) . '</div>';
    }

}