<?php
class Categorized_Products_Slider extends Lmb_Slider {
    const LIMIT_NUMBER_OF_TAGS = 6;
    private $categories;

    protected function set_slider_type() {
        $this->slider_type = 'products';
    }

    public function get_slider_loop_start( $content ) {
        return '<div class="main-slick__choose-brand">';
    }

    public function get_slider_loop_end( $content ) {
        return '</div>';
    }

    protected function the_slider() {
        $this->set_slider_categories();
        $this->set_category_to_slider_options();
        parent::the_slider();
    }

    private function set_slider_categories() {
        $tags_ids = get_terms( array(
            'taxonomy'               => 'product_tag',
            'fields'                 => 'all',
            'orderby'                => 'id',
			'order'                  => 'DESC',
            'meta_key'               => Categorized_Products_Slider_Customizer::POPULAR_SLIDER_META,
            'meta_value'             => '1',
            'meta_compare'           => '=',
            'hide_empty'             => false,
        ));
        $this->categories = $tags_ids;
    }

    private function set_category_to_slider_options() {
        if ( !empty( $this->categories ) && !isset( $this->slider_options['tag'] ) ) {
            $this->slider_options['tag'] = $this->categories[0]->slug;
        }
    }

    protected function slider_content_header() {
        if ( isset( $this->categories ) ) {
        echo "<section>
                <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-12\">
                        <h2 class=\"upper-case\">выбери свой бренд</h2>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-12\">";
                ?>
                        <div class="brands-row__brand-slider">
                        <?php
                            foreach( $this->categories as $c ) {
                                echo "<div class=\"brands-row__brand-wrapper\" id=\"slug-{$c->slug}\">";
                                lmb_subcategory_thumbnail( $c, array( 'class' => 'objectf-contain' ) );
                                echo "</div>";
                            }
                        ?>
                        </div>
                        <div class="brands-row__arrows">
                            <div class="lmb-prev-arrow"><span class="content-wrapper"></span></div>
                            <div class="lmb-next-arrow"><span class="content-wrapper"></span></div>
                        </div>
                <?php
                echo "  </div></div><div class=\"mb-3\"></div>";
        }
    }

    protected function slider_content_footer() {
        $link = get_permalink( wc_get_page_id( 'shop' ) );
        echo "<div class=\"w-100 mb-3\"></div>
                <div class=\"row\">
                    <div class=\"col-12 text-center\">
                        <div class=\"d-inline-block text-center main-slick__all-categs_wrapper\">
                            <a class=\"main-slick__all-categs\" href=\"{$link}\"><span>Посмотреть все продукты</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>";
    }

    static public function ajax_categorized_slider() {
        $slider = new Categorized_Products_Slider(
            array(
                "tag"          => sanitize_text_field( $_REQUEST['category_slug'] ),
                "lmb_slider"   => true, //musthave option
                "echo"         => false,
            )
        );

        wp_send_json(array(
            'slider' => $slider->get_slider_content_without_header_footer()
        ));
    }

}