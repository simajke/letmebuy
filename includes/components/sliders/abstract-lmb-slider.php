<?php
abstract class Lmb_Slider {
    protected $slider_type;
    protected $content_without_woocommerce_wrapper;
    private   $the_slider_content;
    protected $slider_options;

    public function __construct( $options ) {
        $this->set_slider_type();
        $this->set_slider_options( $options );
        if ( !empty( $this->slider_type ) ) {
            $this->init_hooks();
            $this->the_slider();
        }
    }

    protected function init_hooks() {
        add_filter( "shortcode_atts_{$this->slider_type}", array( $this, 'shortcode_atts' ), 10, 4 );
        add_action( "woocommerce_shortcode_before_{$this->slider_type}_loop", array( $this, 'before_products_loop' ), 10, 1 );
        add_action( "woocommerce_shortcode_after_{$this->slider_type}_loop", array( $this, 'after_products_loop' ), 10, 1 );
        add_action( "woocommerce_shortcode_{$this->slider_type}_loop_no_results", array( $this, 'after_products_loop' ), 10, 1 );
    }

    public function shortcode_atts( $out, $pairs, $atts, $shortcode ) {
        if ( isset( $atts["lmb_slider"] ) && boolval( $atts["lmb_slider"] ) === true ) {
            $out["lmb_slider"] = true;
        }
        return $out;
    }

    private function set_slider_options( $options ) {
        $this->slider_options = $options;
    }

    public function before_products_loop( $attributes ) {
        if ( $attributes['lmb_slider'] === true ) {
            add_filter( 'wc_get_template_part', array( $this, 'get_slider_template' ), 10, 3 );
            add_filter( 'woocommerce_currency_symbol', array( $this, 'wrap_currency' ), 10, 2 );
            add_filter( 'woocommerce_product_loop_start', array( $this, 'get_slider_loop_start' ), 10, 1 );
            add_filter( 'woocommerce_product_loop_end', array( $this, 'get_slider_loop_end' ), 10, 1);
            add_action( "after_{$this->slider_type}_slider_initialized", array( $this, 'unset_slider_content'), 10, 1 );
        }
    }

    public function wrap_currency( $currency_symbol, $currency ) {
        return "&nbsp{$currency_symbol}";
    }

    public function after_products_loop( $attributes ) {
        if ( $attributes["lmb_slider"] === true ) {
            if ( $priority = has_action( "wc_get_template_part", array( $this, 'get_slider_template' ) ) ) {
                remove_action( "wc_get_template_part", array( $this, 'get_slider_template' ), $priority );
            }
            if ( $priority = has_action( "woocommerce_product_loop_start", array( $this, 'get_slider_loop_start' ) ) ) {
                remove_action( "woocommerce_product_loop_start", array( $this, 'get_slider_loop_start' ), $priority );
            }
            if ( $priority = has_action( "woocommerce_product_loop_end", array( $this, 'get_slider_loop_end' ) ) ) {
                remove_action( "woocommerce_product_loop_end", array( $this, 'get_slider_loop_end' ), $priority );
            }
            if ( $priority = has_action( 'woocommerce_currency_symbol', array( $this, 'wrap_currency' ) ) ) {
                remove_action( 'woocommerce_currency_symbol', array( $this, 'wrap_currency' ), $priority );
            }
            /**
             * $this->set_slider_content() gets the content before it's beign wrapped in unnecessary
             * <div class="woocommerce columns-{number}">
             */
            $this->set_slider_content();
            add_filter( "do_shortcode_tag", array( $this, 'switch_slider_content' ), 10, 4 );
        }
    }

    private function set_slider_content() {
        $this->content_without_woocommerce_wrapper = !empty( ob_get_contents() ) ? ob_get_contents() : $this->get_empty_result();
    }

    public function switch_slider_content( $output, $tag, $attr, $m ) {
        ob_start();
        $this->slider_content_header();
        $this->the_content();
        $this->slider_content_footer();
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function get_slider_template( $template, $slug, $name ) {
        return locate_template("includes/templates/lmb-slider-template.php");
    }

    protected function the_slider( ) {
        $shortcode_options = '';
        $bool_vals = array( 'false', 'true' );
        foreach ( $this->slider_options as $k => $el ) {
            if ( trim( $k ) == 'echo' ) {
                $echo = $el;
                continue;
            }
            if ( true === $el || false === $el ) {
                $el = $bool_vals[ strval($el) ];
            } else {
                $el = strval($el);
            }
            $shortcode_options .= $k . '=' . $el . ' ';
        }
        $shortcode_options = trim( $shortcode_options );
        $this->the_slider_content = do_shortcode( '[products ' . $shortcode_options . ']' );
        do_action( "after_{$this->slider_type}_slider_initialized" );
        if ( !isset( $echo ) || true == $echo ) {
            echo $this->the_slider_content;
        }
    }

    private function the_content() {
        echo $this->content_without_woocommerce_wrapper;
    }

    abstract protected function set_slider_type();
    abstract protected function slider_content_header();
    abstract protected function slider_content_footer();
    abstract public function get_slider_loop_start( $content );
    abstract public function get_slider_loop_end( $content );

    public function unset_slider_content() {
        if ( $priority = has_action( "do_shortcode_tag", array( $this, 'switch_slider_content' ) ) ) {
            remove_action( "do_shortcode_tag", array( $this, 'switch_slider_content' ), $priority );
        }
    }

    public function get_the_slider_content() {
        return $this->the_slider_content;
    }

    public function get_slider_content_without_header_footer(  ) {
        return $this->content_without_woocommerce_wrapper;
    }

    private function get_empty_result() {
        return "<div class=\"py-5\"><h4 class=\"text-center\">There are no products of such brand</h4></div>";
    }
}