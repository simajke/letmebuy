<?php

/**
 * Class add checkbox in admin panel to the 'Tag' taxonomy
 * by enabling which on term allows the term be shown in the slider.
 */
class Categorized_Products_Slider_Customizer {

    const POPULAR_SLIDER_META = 'tag_popular_slider';

    public function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action( 'edit_term', array( $this, 'create_save_term_meta' ), 10, 3 );
        add_action( 'created_term', array( $this, 'create_save_term_meta' ), 10, 3 );
        add_action( 'add_tag_form_fields', array( $this, 'add_term_to_slider' ) );
        add_action( 'product_tag_edit_form_fields', array( $this, 'edit_term_slider_meta' ), 12, 2 );
    }

    public function add_term_to_slider( $taxonomy ) {
        if ( $taxonomy === 'product_tag' ) {
            ?>
            <div class="form-field term-categorized-slider-wrap">
                <label for="lmb-tag-categorized-slider">
                    <input class="lmb-tag-categorized-slider lmb-checkbox-listener" id="lmb-tag-categorized-slider" type="checkbox">
                    Show term in Categorized products slider
                </label>
                <input name="<?php echo self::POPULAR_SLIDER_META ?>" type="hidden" value="0">
            </div>
                <?php
        }
    }

    public function edit_term_slider_meta( $tag, $taxonomy ) {
        if ( $taxonomy === 'product_tag' ) {
            $term_meta = get_term_meta( $tag->term_id, self::POPULAR_SLIDER_META );
            $tag_categorized_slider = ( ! empty( $term_meta ) && $term_meta[0] ) ? '1' : '0';
            ?>
            <tr class="form-field term-categorized-slider-wrap">
                <th scope="row" valign="top"><label for="lmb-tag-categorized-slider"><?php esc_html_e( 'Show term in Categorized products slider', 'lmb' ); ?></label></th>
                <td>
                    <input class="lmb-tag-categorized-slider lmb-checkbox-listener" id="lmb-tag-categorized-slider" type="checkbox"
                        <?php echo ( $tag_categorized_slider == true ) ? "checked" : "" ?>>
                    <input name="<?php echo self::POPULAR_SLIDER_META ?>" type="hidden" value="<?php echo $tag_categorized_slider; ?>">
                </td>
            </tr>
            <?php
        }
    }

    public function create_save_term_meta( $term_id, $tt_id = '', $taxonomy = '' ) {
        if ( isset( $_POST[ self::POPULAR_SLIDER_META ] ) && 'product_tag' === $taxonomy ) {
            update_term_meta( $term_id, self::POPULAR_SLIDER_META, absint( $_POST[ self::POPULAR_SLIDER_META ] ) );
        }
    }
}