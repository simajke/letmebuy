<?php
class Popular_Products_Slider extends Lmb_Slider {

    protected function set_slider_type() {
        $this->slider_type = 'best_selling_products';
    }

    public function get_slider_loop_start( $content ) {
        return '<div class="main-slick__buyers-choice">';
    }

    public function get_slider_loop_end( $content ) {
        return '</div>';
    }

    protected function slider_content_header() {
        echo "<section>
                <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-12\">
                        <h2 class=\"mb-4 upper-case\">хиты продаж</h2>
                    </div>
                </div>";
    }

    protected function slider_content_footer() {
        $link = add_query_arg( 'orderby', 'popularity', get_permalink( wc_get_page_id( 'shop' ) ) );
            echo "<div class=\"w-100 mb-3\"></div>
                    <div class=\"row\">
                        <div class=\"col-12 text-center\">
                            <div class=\"d-inline-block text-center main-slick__all-categs_wrapper\">
                                <a class=\"main-slick__all-categs\" href=\"{$link}\"><span>Посмотреть все популярные продукты</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>";
    }

}