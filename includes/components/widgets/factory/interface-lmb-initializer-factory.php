<?php
interface LMB_Initializer_Factory_Interface {
    public function make( $initializer );
}