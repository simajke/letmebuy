<?php
class LMB_Initializer_Factory implements LMB_Initializer_Factory_Interface {
    const PREFIX   = 'LMB_';
    const POSTFIX  = '_Initializer';

    public function make( $initializer ) {
        if ( empty( $initializer ) ) {
            return null;
        }

        $classname = self::PREFIX . $initializer . self::POSTFIX;

        if ( class_exists( $classname ) ) {
            return new $classname();
        }

    }
}