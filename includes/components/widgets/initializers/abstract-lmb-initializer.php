<?php
abstract class LMB_Initializer_Abstract {

    public function __construct() {
        $this->init_widget();
    }

    protected function init_widget() {
        $this->register_widget();
    }

    protected function register_widget() {
        register_widget( $this::WIDGET );
    }
}