<?php
defined( 'ABSPATH' ) || exit;

class LMB_Filter_Tag_Initializer extends LMB_Initializer_Abstract {
    const WIDGET = 'LMB_Filter_Tag';

    protected function init_widget() {
        $this->set_hooks();
        parent::register_widget();
    }

    private function set_hooks() {
        add_action( 'pre_get_posts', array( LMB()->query, 'set_tag_in_query' ) );
    }
}