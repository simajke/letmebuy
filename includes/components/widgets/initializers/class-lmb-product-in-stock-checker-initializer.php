<?php
defined( 'ABSPATH' ) || exit;
/**
 * Class to register default theme widget and its' hooks.
 * Register when woocommerce is active.
 */
class LMB_Product_In_Stock_Checker_Initializer extends LMB_Initializer_Abstract {
    const WIDGET = 'LMB_Product_In_Stock_Checker';

    protected function init_widget() {
        $this->set_hooks();
        parent::register_widget();
    }

    private function set_hooks(){
        add_action( 'pre_get_posts', array( LMB()->query, 'add_query_on_availability' ) );
        // add_filter( 'woocommerce_price_filter_sql', array( LMB()->query, 'add_query_part_wc_price_filter' ), 10, 3 );
        // add_filter( 'get_meta_sql', array( LMB()->query, 'add_meta_part_on_availability' ), 10, 6 );

        // add_filter( 'posts_clauses', array( LMB()->query, 'add_query_part_min_max_price' ), 20, 2 );
    }
}