<?php
defined( 'ABSPATH' ) || exit;

class LMB_Filter_Tag extends WP_Widget {

    public function __construct() {
        $id_base        = 'lmb-filter-manufacturer';
        $name           = 'Filter by Manufacturer( LMB )';
        $widget_options = array(
            'description' => 'Filter by Manufacturer( LMB )'
        );
        parent::__construct(
            $id_base,
            $name,
            $widget_options
        );
    }

    public function widget( $args, $instance ) {
        extract( $args );
        $title = esc_html( $instance['title'] );
        $tags = get_terms( array(
            'taxonomy'               => 'product_tag',
            'fields'                 => 'all',
            'orderby'                => 'name',
            'order'                  => 'ASC',
            'meta_query'             => array(
                array(
                    'key'    => LMB_Filter_Tag_Customizer::ENABLE_FILTERING_META,
                    'value'  => 0,
                    'compare'=> '>',
                    'type'   => 'numeric'
                )
            )
        ));

        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <div class="lmb-filter-tag__tags-wrapper">
            <?php
                if ( isset( $_GET['manufact'] ) && ! empty( $_GET['manufact'] ) ) {
                    $categ_id = lmb_sanitize_text_field( $_GET['manufact'] );
                }
                foreach( $tags as $c ) {
                    ?>
                    <div class="lmb-checkbox__wrapper">

                        <input id="tag-<?php echo esc_html( $c->term_id ); ?>" type="checkbox" name="manufact[]" value="<?php echo esc_html( $c->term_id ); ?>"
                        <?php
                            if ( isset( $categ_id )
                                    && ( is_array( $categ_id ) && in_array( $c->term_id, $categ_id )
                                            || $c->term_id == $categ_id ) ) {
                                                echo "checked";
                                            }
                        ?>
                        >
                        <?php echo esc_html( $c->name ); ?>
                        <label for="tag-<?php echo esc_html( $c->term_id ); ?>"></label>
                    </div>
                    <?php
                }
            ?>
        </div>
        <?php
        echo $after_widget;
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = 'Filter by Manufacturer';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
         </p>
    <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

    public function get_select_options() {
        return $this->select_options;
    }

}