<?php
defined( 'ABSPATH' ) || exit;

class LMB_Filter_Categories extends WP_Widget {

    public function __construct() {
        $id_base        = 'lmb-filter-categories';
        $name           = 'Filter by Categories( LMB )';
        $widget_options = array(
            'description' => 'Filter by Categories( LMB )'
        );
        parent::__construct(
            $id_base,
            $name,
            $widget_options
        );
    }

    public function widget( $args, $instance ) {
        extract( $args );
        $title = esc_html( $instance['title'] );
        $categories = get_terms( array(
            'taxonomy'               => 'product_cat',
            'fields'                 => 'all',
            'orderby'                => 'name',
            'order'                  => 'ASC',
            'exclude'                => array(15),
        ));

        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <div class="lmb-filter-categories__categ-wrapper">
            <?php
                if ( isset( $_GET['category'] ) && ! empty( $_GET['category'] ) ) {
                    $categ_id = lmb_sanitize_text_field( $_GET['category'] );
                }
                foreach( $categories as $c ) {
                    ?>
                    <div class="lmb-checkbox__wrapper">
                        <input id="category-<?php echo esc_html( $c->term_id ); ?>" type="checkbox" name="category[]" value="<?php echo esc_html( $c->term_id ); ?>"
                        <?php
                            if ( isset( $categ_id )
                                    && ( is_array( $categ_id ) && in_array( $c->term_id, $categ_id )
                                            || $c->term_id == $categ_id ) ) {
                                                echo "checked";
                                            }
                        ?>
                        >
                        <?php echo esc_html( $c->name ); ?>
                        <label for="category-<?php echo esc_html( $c->term_id ); ?>"></label>
                    </div>
                    <?php
                }
            ?>
        </div>
        <?php
        echo $after_widget;
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = 'Filter by Categories';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
         </p>
    <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

    public function get_select_options() {
        return $this->select_options;
    }

}