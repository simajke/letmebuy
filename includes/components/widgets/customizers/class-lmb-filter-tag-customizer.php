<?php
defined( 'ABSPATH' ) || exit;
/**
 *
 */
class LMB_Filter_Tag_Customizer {

    const ENABLE_FILTERING_META = 'tag_enable_filtering';

    public function __construct() {
        $this->set_hooks();
    }


    private function set_hooks() {
        add_action( 'edit_term', array( $this, 'create_and_save_term' ), 10, 3 );
        add_action( 'created_term', array( $this, 'create_and_save_term' ), 10, 3 );
        add_action( 'add_tag_form_fields', array( $this, 'add_term_filter' ) );
        add_action( 'product_tag_edit_form_fields', array( $this, 'edit_term_filter' ), 11, 2 );
    }

    public function create_and_save_term( $term_id, $tt_id = '', $taxonomy = '' ) {
        if ( isset( $_POST[ self::ENABLE_FILTERING_META ] ) && 'product_tag' === $taxonomy ) {
            update_term_meta( $term_id, self::ENABLE_FILTERING_META, absint( $_POST[ self::ENABLE_FILTERING_META ] ) );
        }
    }

    public function add_term_filter( $taxonomy ) {
        if ( $taxonomy === 'product_tag' ) {
            ?>
            <div class="form-field term-filtering-wrap">
                <label for="lmb-tag-filtering">
                    <input class="lmb-tag-filtering lmb-checkbox-listener" id="lmb-tag-filtering" type="checkbox">
                    Activate tag in filter
                </label>
                <input name="<?php echo self::ENABLE_FILTERING_META ?>" type="hidden" value="0">
            </div>
                <?php
        }
    }

    public function edit_term_filter( $tag, $taxonomy ) {
        if ( $taxonomy === 'product_tag' ) {
            $term_meta = get_term_meta( $tag->term_id, self::ENABLE_FILTERING_META );
            $tag_filtering = ( ! empty( $term_meta ) && $term_meta[0] ) ? '1' : '0';
            ?>
            <tr class="form-field term-filtering-wrap">
                <th scope="row" valign="top">
                    <label for="lmb-tag-filtering">
                        <?php esc_html_e( 'Activate tag in filter', 'lmb' ); ?>
                    </label>
                </th>
                <td>
                    <input class="lmb-tag-filtering lmb-checkbox-listener" id="lmb-tag-filtering" type="checkbox"
                        <?php echo ( $tag_filtering == true ) ? "checked" : "" ?>>
                    <input name="<?php echo self::ENABLE_FILTERING_META ?>" type="hidden" value="<?php echo $tag_filtering; ?>">
                </td>
            </tr>
                <?php
        }
    }
}