<?php
defined( 'ABSPATH' ) || exit;

class LMB_Product_In_Stock_Checker extends WP_Widget {
    private $select_options = array(
        'in_stock'           => 'Продукты в наличии',
        'in_stock_for_order' => 'Продукты в наличии и под заказ',
        'all_products'       => 'Все товары, включая отсутствующие в продаже'
    );

    public function __construct() {
        $id_base        = 'lmb-product-in-stock-checker';
        $name           = 'Products in stock checker';
        $widget_options = array(
            'description' => 'LMB_Product_In_Stock_Checker filters products in stock'
        );
        parent::__construct(
            $id_base,
            $name,
            $widget_options
        );
        wp_enqueue_style( 'widget-product-checker', lmb_get_manifest_data_single('stockChecker.css') );
        wp_enqueue_script( 'widget-product-checker', lmb_get_manifest_data_single('stockChecker.js'), array(), '1.0.0', true );

    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget( $args, $instance ) {
        extract( $args );

        if ( !isset( $instance['title'] ) ) {
            $instance['title'] = 'Наличие в магазине';
        }

        $title = esc_html( $instance['title'] );
        $widget_options      = apply_filters( 'lmb-products-in-stock-options', $this->select_options );
        $name_value          = 'products_availability';
        $options_wrapper_id  = 'lmb-product-availability';

        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }
        if ( $instance['widget_view'] === 'show_as_dropdown' ) {
            $widget_options      = array( 'default' => 'Выберите пункт' ) + $widget_options;
            echo '<select name="' . $name_value . '" id="' . $options_wrapper_id . '">';
            foreach( $widget_options as $key => $option ) {

                $option_class   = 'lmb_products_availability__option';

                if ( $key == 'default' )
                    $key = '';
                else
                    $option_class .= ' lmb_products_availability__clickable';

                echo '<option class="'. $option_class .'" value="' . esc_html( $key ) . '">' . esc_html( $option ) . '</option>';
            }
            echo '</select>';
        }

        if ( $instance['widget_view'] === 'show_as_radio' ) {
            ?>
            <div id="<?php echo $options_wrapper_id ?>" class="<?php echo $options_wrapper_id . '__radio' ?>">
                <?php
                    foreach( $widget_options as $key => $option ) {

                        $option_class   = 'lmb_products_availability__option';

                        if ( $key == 'default' )
                            $key = '';
                        else
                            $option_class .= ' lmb_products_availability__clickable';

                ?>
                        <div class="lmb-radio__wrapper">
                            <input class="<?php echo $option_class ?>"
                                id="<?php echo esc_attr( $key ); ?>"
                                type="radio"
                                name="<?php echo esc_attr( $name_value ); ?>"
                                value="<?php echo esc_attr( $key ) ?>"
                                <?php echo ( isset( $_GET['products_availability'] ) && $key === $_GET['products_availability'] )
                                    ? 'checked' : ( ( ! isset( $_GET['products_availability'] ) && $key === 'all_products' ) ? 'checked' : '' ); ?>>
                            <?php echo esc_html( $option ); ?>
                            <label for="<?php echo esc_attr( $key ); ?>"></label>
                        </div>
                <?php
                    }
                ?>
            </div>
            <?php
        }
        echo $after_widget;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = 'Letmebuy in Stock Checker';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat"
                id="<?php echo $this->get_field_id( 'title' ); ?>"
                name="<?php echo $this->get_field_name( 'title' ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_name( 'show_as_dropdown' ); ?>"><?php _e( 'Show as dropdown:' ); ?></label>
            <input class="widefat"
                id="<?php echo $this->get_field_id( 'show_as_dropdown' ); ?>"
                name="<?php echo $this->get_field_name( 'widget_view' ); ?>"
                type="radio" value="show_as_dropdown"
                <?php echo ( ! isset( $instance['widget_view'] ) || $instance['widget_view'] === 'show_as_dropdown' ) ? 'checked' : ''; ?>/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_name( 'show_as_radio' ); ?>"><?php _e( 'Show as radio buttons:' ); ?></label>
            <input class="widefat"
                id="<?php echo $this->get_field_id( 'show_as_radio' ); ?>"
                name="<?php echo $this->get_field_name( 'widget_view' ); ?>"
                type="radio"
                value="show_as_radio"
                <?php echo ( isset( $instance['widget_view'] ) && $instance['widget_view'] === 'show_as_radio' ) ? 'checked' : ''; ?>/>
        </p>
    <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['widget_view'] = ( isset( $new_instance['widget_view'] ) && !empty( $new_instance['widget_view'] ) ) ? strip_tags( $new_instance['widget_view'] ) : '';
        return $instance;
    }

    public function get_select_options() {
        return $this->select_options;
    }
}