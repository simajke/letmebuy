<?php


class LMB_Styles extends WP_Styles {
//    private $upper_most_parent;

    public function __construct() {
        parent::__construct();
//        $this->init_upper_most_parent();
    }

    public function is_registered_handle( $handle, $group = false ) {
        return isset( $this->registered[ $handle ] );
    }
//    private function init_upper_most_parent() {
//        $this->upper_most_parent = new WP_Dependencies();
//    }
    /**
     * Holds a string which contains the type attribute for style tag.
     *
     * If the current theme does not declare HTML5 support for 'style',
     * then it initializes as `type='text/css'`.
     *
     * @since 5.3.0
     * @var string
     */
    protected $type_attr;

    public function do_item( $handle, $group = false ) {
        if ( ! $this->is_registered_handle( $handle ) ) {
            return false;
        }

        do_action( 'lmb_forming_tag_start', $handle );

        $obj = $this->registered[ $handle ];

        if ( null === $obj->ver ) {
            $ver = '';
        } else {
            $ver = $obj->ver ? $obj->ver : $this->default_version;
        }

        if ( isset( $this->args[ $handle ] ) ) {
            $ver = $ver ? $ver . '&amp;' . $this->args[ $handle ] : $this->args[ $handle ];
        }

        $src         = $obj->src;
        $cond_before = '';
        $cond_after  = '';
        $conditional = isset( $obj->extra['conditional'] ) ? $obj->extra['conditional'] : '';

        if ( $conditional ) {
            $cond_before = "<!--[if {$conditional}]>\n";
            $cond_after  = "<![endif]-->\n";
        }

        $inline_style = $this->print_inline_style( $handle, false );

        if ( $inline_style ) {
            $inline_style_tag = sprintf(
                "<style id='%s-inline-css'%s>\n%s\n</style>\n",
                esc_attr( $handle ),
                $this->type_attr,
                $inline_style
            );
        } else {
            $inline_style_tag = '';
        }

        if ( $this->do_concat ) {
            if ( $this->in_default_dir( $src ) && ! $conditional && ! isset( $obj->extra['alt'] ) ) {
                $this->concat         .= "$handle,";
                $this->concat_version .= "$handle$ver";

                $this->print_code .= $inline_style;

                return true;
            }
        }

        if ( isset( $obj->args ) ) {
            $media = esc_attr( $obj->args );
        } else {
            $media = 'all';
        }

        // A single item may alias a set of items, by having dependencies, but no source.
        if ( ! $src ) {
            if ( $inline_style_tag ) {
                if ( $this->do_concat ) {
                    $this->print_html .= $inline_style_tag;
                } else {
                    echo $inline_style_tag;
                }
            }

            return true;
        }

        $href = $this->_css_href( $src, $ver, $handle );
        if ( ! $href ) {
            return true;
        }

        $rel   = isset( $obj->extra['alt'] ) && $obj->extra['alt'] ? 'alternate stylesheet' : 'stylesheet';
        $title = isset( $obj->extra['title'] ) ? sprintf( "title='%s'", esc_attr( $obj->extra['title'] ) ) : '';

        $tag = sprintf(
            "<link rel='%s' id='%s-css' %s href='%s'%s media='%s' />\n",
            $rel,
            $handle,
            $title,
            $href,
            $this->type_attr,
            $media
        );

        /**
         * Filters the HTML link tag of an enqueued style.
         *
         * @since 2.6.0
         * @since 4.3.0 Introduced the `$href` parameter.
         * @since 4.5.0 Introduced the `$media` parameter.
         *
         * @param string $html   The link tag for the enqueued style.
         * @param string $handle The style's registered handle.
         * @param string $href   The stylesheet's source URL.
         * @param string $media  The stylesheet's media attribute.
         */
        $tag = apply_filters( 'style_loader_tag', $tag, $handle, $href, $media );

        if ( 'rtl' === $this->text_direction && isset( $obj->extra['rtl'] ) && $obj->extra['rtl'] ) {
            if ( is_bool( $obj->extra['rtl'] ) || 'replace' === $obj->extra['rtl'] ) {
                $suffix   = isset( $obj->extra['suffix'] ) ? $obj->extra['suffix'] : '';
                $rtl_href = str_replace( "{$suffix}.css", "-rtl{$suffix}.css", $this->_css_href( $src, $ver, "$handle-rtl" ) );
            } else {
                $rtl_href = $this->_css_href( $obj->extra['rtl'], $ver, "$handle-rtl" );
            }

            $rtl_tag = sprintf(
                "<link rel='%s' id='%s-rtl-css' %s href='%s'%s media='%s' />\n",
                $rel,
                $handle,
                $title,
                $rtl_href,
                $this->type_attr,
                $media
            );

            /** This filter is documented in wp-includes/class.wp-styles.php */
            $rtl_tag = apply_filters( 'style_loader_tag', $rtl_tag, $handle, $rtl_href, $media );

            if ( 'replace' === $obj->extra['rtl'] ) {
                $tag = $rtl_tag;
            } else {
                $tag .= $rtl_tag;
            }
        }

        if ( $this->do_concat ) {
            $this->print_html .= $cond_before;
            $this->print_html .= $tag;
            if ( $inline_style_tag ) {
                $this->print_html .= $inline_style_tag;
            }
            $this->print_html .= $cond_after;
        } else {
            echo $cond_before;
            echo $tag;
            $this->print_inline_style( $handle );
            echo $cond_after;
        }

        do_action( 'lmb_forming_tag_end', $handle );

        return true;
    }

}