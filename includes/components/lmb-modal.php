<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="trigger-test" data-toggle="modal" data-target="#staticBackdrop">
</button>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body">
                <input id="lmb-product-search" class="lmb-product-search" type="text" placeholder="Я хочу купить..." tabindex="-1" autocomplete="off">
                <span id="lmb-product-search__clear" class="d-none justify-content-center align-items-center absolute-center-right lmb-product-search__clear">
                    <img src="<?php echo lmb_get_manifest_data_single("img/close.png"); ?>" alt="">
                </span>
            </div>
            <div id="lmb-search-response" class="d-none modal-footer">

            </div>
        </div>
    </div>
</div>