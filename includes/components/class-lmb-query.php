<?php
defined( 'ABSPATH' ) || exit;

class LMB_Query {

    public function __construct() {
        $this->set_hooks();
    }

    private function set_hooks() {
        add_action( 'pre_get_posts', array( $this, 'set_default_categories' ) );
    }
    /**
     * The function works during 'pre_get_posts' which allows to check $_GET parameter
     * and depends on its' value set meta query. By doing it during 'posts_clauses'
     * hook it's already too late.
     */
    public function add_query_on_availability( $wp_query ) {

        if ( ! $wp_query->is_main_query() || ! isset( $_GET['products_availability'] ) ) {
            return;
        }

        $widget_product_in_stock_checker = lmb_get_widget_instance( LMB_Product_In_Stock_Checker_Initializer::WIDGET );

        if ( ! isset( $widget_product_in_stock_checker ) ) {
            return ;
        }

        $allowed_values = array_keys( $widget_product_in_stock_checker->get_select_options() );

        $availability_option = wc_clean( wp_unslash( $_GET['products_availability'] ) );

        if ( in_array( $availability_option, $allowed_values ) ) {

            if (  $availability_option === 'all_products' ) {
                return;
            }

            // if ( $availability_option === 'in_stock_for_order' ) {
            //     $meta_args = $wp_query->get( 'meta_query' );
            //     if ( empty( $meta_args ) || ! isset( $meta_args['relation'] ) ) {
            //         $meta_query = array( 'relation' => 'AND' );
            //     }
            //     $meta_query[] = array(
            //             'key'     => 'in_stock_for_order',
            //             'value'   => '1',
            //             'compare' => '=',
            //             'type'    => 'numeric'
            //         );
            //     $wp_query->set( 'meta_query', $meta_query );
            // }

            add_filter( 'posts_clauses', array( $this, 'add_query_part_on_availability'), 10, 2 );
        }
    }

    // public function add_meta_part_on_availability( $sql, $queries, $type, $primary_table, $primary_id_column, $context ) {
    //     if ( empty( $context ) || !$context->is_main_query() ) {
    //         return $sql;
    //     }

    //     if ( isset( $_GET['products_availability'] ) && $_GET['products_availability'] == 'in_stock_for_order' ) {
    //         preg_match_all('/(\b[^AND][^OR]\w.*\b["\']?)/m', $sql['where'], $matches, PREG_PATTERN_ORDER ); //need more test
    //         if ( isset( $matches[1] ) && ! empty( $matches[1] ) ) {
    //             foreach ( $matches[1] as &$m ) {
    //                 if ( strpos( $m, 'in_stock_for_order' ) !== false ) {
    //                     $m = '( ' . $m . ' ) OR wc_product_meta_lookup.stock_status = "onbackorder"';
    //                 }
    //             }
    //             unset( $m );
    //             $sql['where'] = ' AND ( ( ' . implode( ' ) AND ( ', $matches[1] ) . ' ) )';
    //         }
    //     }

    //     return $sql;
    // }
    // public function add_meta_part_on_availability( $sql, $queries, $type, $primary_table, $primary_id_column, $context ) {
    //     if ( empty( $context ) || !$context->is_main_query() ) {
    //         return $sql;
    //     }

    //     if ( isset( $_GET['products_availability'] ) && $_GET['products_availability'] == 'in_stock_for_order' ) {
    //         preg_match_all('/(\b[^AND][^OR]\w.*\b["\']?)/m', $sql['where'], $matches, PREG_PATTERN_ORDER ); //need more test
    //         if ( isset( $matches[1] ) && ! empty( $matches[1] ) ) {
    //             $parts = $this->get_where_values_on_availability();
    //             foreach ( $matches[1] as &$m ) {
    //                 if ( strpos( $m, 'in_stock_for_order' ) !== false ) {
    //                     $m = '( ' . $m . ' ) OR wc_product_meta_lookup.stock_quantity '. $parts['where'];
    //                 }
    //             }
    //             unset( $m );
    //             $sql['where'] = ' AND ( ( ' . implode( ' ) AND ( ', $matches[1] ) . ' ) )';
    //         }
    //     }

    //     return $sql;
    // }

    public function add_query_part_on_availability( $args ) {

        if ( ! isset( $_GET['products_availability'] )  ) {
            return $args;
        }

        $availability_option = wc_clean( wp_unslash( $_GET['products_availability'] ) );

        $widget_product_in_stock_checker = lmb_get_widget_instance( LMB_Product_In_Stock_Checker_Initializer::WIDGET );
        $allowed_values = array_keys( $widget_product_in_stock_checker->get_select_options() );

        if ( ! isset( $widget_product_in_stock_checker ) || ! in_array( $availability_option, $allowed_values ) ) {
            return $args;
        }

        $wc_query = WC()->query;

        $reflectorWcQuery = new ReflectionClass( $wc_query );
        $reflectorMethod = $reflectorWcQuery->getMethod( 'append_product_sorting_table_join' );
        $reflectorMethod->setAccessible( true );
        $args['join'] = $reflectorMethod->invoke( $wc_query, $args['join'] );


        // $parts = $this->get_where_values_on_availability();

        if ( $availability_option == 'in_stock' ) {
            $args['where'] .= ' AND wc_product_meta_lookup.stock_status = "instock"';
        }

        if ( $availability_option == 'in_stock_for_order' ) {
            $args['where'] .= ' AND wc_product_meta_lookup.stock_status IN ( "instock", "onbackorder" )';
        }


        return $args;
    }

    // public function add_query_part_wc_price_filter( $sql, $meta_query_sql, $tax_query_sql ) {

    //     if ( ! isset( $_GET['products_availability'] )  ) {
    //         return $sql;
    //     }

    //     $availability_option = wc_clean( wp_unslash( $_GET['products_availability'] ) );
    //     $parts = $this->get_where_values_on_availability();

    //     if ( $availability_option === 'in_stock' ) {
    //         $sql .= ' AND stock_quantity > ' . $parts['where'] . ' ';
    //     }

    //     if ( $availability_option === 'in_stock_for_order' ) {
    //         $sql .= ' OR stock_quantity ' . $parts['where'] . ' ';
    //     }

    //     return $sql;
    // }

    // public function get_where_values_on_availability() {

    //     $availability_option = wc_clean( wp_unslash( $_GET['products_availability'] ) );

    //     if ( ! isset( $availability_option ) ) {
    //         return;
    //     }

    //     $parts = array();

    //     if ( $availability_option === 'in_stock' ) {
    //         $parts['where'] = 0;
    //     }

    //     if ( $availability_option === 'in_stock_for_order' ) {
    //         $parts['where'] = 'IS NOT NULL';
    //     }

    //     return $parts;
    // }

    public function add_query_part_min_max_price( $piecies, $wp_query ) {

        if ( ! isset( $_GET['min_price'] ) || ! isset( $_GET['max_price'] ) || ! isset( $_GET['products_availability'] ) ) {
            return $piecies;
        }

        $availability_option = wc_clean( wp_unslash( $_GET['products_availability'] ) );

        if ( $availability_option !== 'in_stock_for_order' ) {
            return $piecies;
        }

        $main_query = WC()->query->get_main_query();
        $price_query_parts = WC()->query->price_filter_post_clauses( array('where' => '', 'join' => ''), $main_query );
        $where_part = str_replace( 'wc_product_meta_lookup.', '', $price_query_parts['where'] );
        $piecies['where'] .= $where_part;

        return $piecies;
    }

    public function set_default_categories( $wp_query ) {

        if ( ! $wp_query->is_main_query() || is_admin() ) {
            return;
        }

        // if ( ! is_shop() && ! is_product_taxonomy() ) {
		// 	return;
        // }

        if ( ! is_array( $tax_query = $wp_query->get('tax_query') ) ) {
			$tax_query = array(
                'relation' => 'AND',
			);
        }

        $tax_query[] = array(
            'taxonomy' => 'product_cat',
            'field'    => 'term_id',
            'terms'    => 15,
            'operator' => 'NOT IN'
        );

        $wp_query->set( 'tax_query', $tax_query );
    }

    public function set_category_in_query( $wp_query ) {

        if ( ! $wp_query->is_main_query()  || ! isset( $_GET['category'] ) ) {
            return;
        }

        if ( ! is_array( $tax_query = $wp_query->get('tax_query') ) ) {
			$tax_query = array(
                'relation' => 'AND',
			);
        }

        $user_values = lmb_sanitize_text_field( wp_unslash( $_GET['category'] ) );

        $tax_query[] = array(
            'taxonomy' => 'product_cat',
            'field'    => 'term_id',
            'terms'    => $user_values,
            'operator' => 'IN'
        );

        $wp_query->set( 'tax_query', $tax_query );
    }

    public function set_tag_in_query( $wp_query ) {

        if ( ! $wp_query->is_main_query()  || ! isset( $_GET['manufact'] ) ) {
            return;
        }

        if ( ! is_array( $tax_query = $wp_query->get('tax_query') ) ) {
			$tax_query = array(
                'relation' => 'AND',
			);
        }

        $user_values = lmb_sanitize_text_field( wp_unslash( $_GET['manufact'] ) );

        $tax_query[] = array(
            'taxonomy' => 'product_tag',
            'field'    => 'term_id',
            'terms'    => $user_values,
            'operator' => 'IN'
        );

        $wp_query->set( 'tax_query', $tax_query );
    }
}